package io.ibj.PixelCore.listener;

import io.ibj.PixelCore.PixelCore;
import io.ibj.PixelCore.player.Stats;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * @author joe 2/22/2015
 */
public class ChatListener implements Listener {

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e){
        int prestige = PixelCore.getI().getPlayerManager().getPlayer(e.getPlayer()).getAttribute(Stats.class).getPrestige();
        e.setFormat(e.getFormat().replaceAll("<prestige>",prestige != 0 ? PixelCore.getI().getCachedConfig().getPrestigeFormat().replaceAll("<prestige>", String.valueOf(prestige)):""));
    }

}
