package io.ibj.PixelCore.listener;

import org.bukkit.Material;
import org.bukkit.block.Biome;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPhysicsEvent;

/**
 * Created by joe on 12/23/2014.
 */
public class BedFixListener implements Listener {

    @EventHandler
    public void onBedExplode(BlockPhysicsEvent e){
        if(e.getBlock().getType()== Material.BED_BLOCK){
            if(e.getBlock().getBiome() == Biome.HELL){
                e.setCancelled(true);
            }
        }
    }

}
