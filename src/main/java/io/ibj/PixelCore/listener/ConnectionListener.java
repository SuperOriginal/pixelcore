package io.ibj.PixelCore.listener;

import com.earth2me.essentials.Essentials;
import com.earth2me.essentials.spawn.EssentialsSpawn;
import io.ibj.JLib.ThreadLevel;
import io.ibj.JLib.TimePeriod;
import io.ibj.JLib.TimeUnit;
import io.ibj.PixelCore.PixelCore;
import io.ibj.PixelCore.TabListEntry;
import io.ibj.PixelCore.player.APlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * @author joe 1/22/2015
 */
public class ConnectionListener implements Listener {
    
    @EventHandler
    public void onJoin(final PlayerJoinEvent e){
        TabListEntry bestFit = null;
        for(TabListEntry entry : PixelCore.getI().getCachedConfig().getTabEntries()){
            if(entry.hasPerm(e.getPlayer())){
                if(bestFit != null){
                    if(bestFit.getWeight() < entry.getWeight()){
                        bestFit = entry;
                    }
                }
                else
                {
                    bestFit = entry;
                }
            }
        }
        if(bestFit != null){
            final TabListEntry finalBestFit = bestFit;
            PixelCore.getI().runLater(new Runnable() {
                @Override
                public void run() {
                    finalBestFit.apply(e.getPlayer());
                }
            },ThreadLevel.SYNC,new TimePeriod(.5, TimeUnit.SECONDS));
            bestFit.apply(e.getPlayer());
        }

        e.getPlayer().teleport(EssentialsSpawn.getPlugin(EssentialsSpawn.class).getSpawn(Essentials.getPlugin(Essentials.class).getUser(e.getPlayer()).getGroup()));
    }
    
    @EventHandler
    public void onLockJoin(PlayerJoinEvent e){
        if(PixelCore.getI().getLockManager().isStartupLocked()){
            e.getPlayer().kickPlayer(PixelCore.getI().getF("locks.starting").constructFancyMessage().get(0).toOldMessageFormat());

        }
        else if(PixelCore.getI().getLockManager().isRestartLocked()){
            e.getPlayer().kickPlayer(PixelCore.getI().getF("locks.restarting").constructFancyMessage().get(0).toOldMessageFormat());
        }
    }
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void onAttemptJoin(AsyncPlayerPreLoginEvent e){
        if(e.getLoginResult() != AsyncPlayerPreLoginEvent.Result.ALLOWED){
            return;
        }
        PixelCore.getI().getPlayerManager().onJoin(e.getUniqueId(),e.getName(),e.getAddress().toString().replace("/",""));
    }
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void onLeave(final PlayerQuitEvent e){
        PixelCore.getI().runNow(new Runnable() {
            @Override
            public void run() {
                APlayer player = PixelCore.getI().getPlayerManager().getPlayer(e.getPlayer());
                PixelCore.getI().getStatsManager().saveAPlayer(player);
                PixelCore.getI().getPlayerManager().onLeave(e.getPlayer());
            }
        }, ThreadLevel.ASYNC);
    }
    @EventHandler(priority = EventPriority.MONITOR)
    public void onLeave(final PlayerKickEvent e){
        PixelCore.getI().runNow(new Runnable() {
            @Override
            public void run() {
                APlayer player = PixelCore.getI().getPlayerManager().getPlayer(e.getPlayer());
                PixelCore.getI().getStatsManager().saveAPlayer(player);
                PixelCore.getI().getPlayerManager().onLeave(e.getPlayer());
            }
        }, ThreadLevel.ASYNC);
    }
}
