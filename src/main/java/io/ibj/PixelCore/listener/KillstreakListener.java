package io.ibj.PixelCore.listener;

import io.ibj.PixelCore.CachedConfig;
import io.ibj.PixelCore.PixelCore;
import lombok.Data;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;

import java.util.*;

/**
 * @author joe 3/3/2015
 */
public class KillstreakListener implements Listener {


    Map<UUID, KillReport> killReports = new HashMap<>();

    @EventHandler
    public void onKill(PlayerDeathEvent e){
        killReports.remove(e.getEntity().getUniqueId());
        List<MetadataValue> data =  e.getEntity().getMetadata("lastPlayerDamagerCause");
        if(data == null || data.size() == 0){
            return;
        }
        EntityDamageEvent lastDamageCause = e.getEntity().getLastDamageCause();
        if(lastDamageCause == null){
            return;
        }
        if(!Objects.equals(data.get(0).asString(), lastDamageCause.getCause().name())){ //Not last damage cause
            return;
        }
        data =  e.getEntity().getMetadata("lastPlayerDamager");
        if(data == null || data.size() == 0){
            return;
        }
        UUID id = (UUID) data.get(0).value();
        if(id != null){
            Player player = Bukkit.getPlayer(id);
            if(player == null){
                return;
            }
            KillReport killReport = killReports.get(id);
            if(killReport == null){
                killReport = new KillReport();
                killReports.put(id,killReport);
            }
            if(killReport.getLastKill() < System.currentTimeMillis()-PixelCore.getI().getCachedConfig().getKillstreakTimeout()*10000){ //Timeout/new
                killReport.setKills(0);
            }
            killReport.setLastKill(System.currentTimeMillis());
            killReport.setKills(killReport.getKills()+1);
            for(CachedConfig.KillstreakEntry entry : PixelCore.getI().getCachedConfig().getKillstreakEntries()){
                if(entry.getAmount() == killReport.getKills()){
                    PixelCore.getI().getPlayerManager().getPlayer(player).getTitleManager().sendMovingHologram(entry.getMessage(),e.getEntity().getLocation());
                    break;
                }
            }
        }
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent e){
        killReports.remove(e.getPlayer().getUniqueId());
    }

    @EventHandler
    public void onKick(PlayerKickEvent e){
        killReports.remove(e.getPlayer().getUniqueId());
    }

    @Data
    public static class KillReport{
        int kills = 0;
        long lastKill = 0;
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void lastDamageEvent(EntityDamageByEntityEvent e){
        if(e.getEntity() instanceof Player){
            if(e.getDamager() instanceof Player){
                e.getEntity().setMetadata("lastPlayerDamager_core",new FixedMetadataValue(PixelCore.getI(),e.getDamager().getUniqueId()));
                e.getEntity().setMetadata("lastPlayerDamagerCause_core",new FixedMetadataValue(PixelCore.getI(),e.getCause().name()));
            }
            else if(e.getDamager() instanceof Projectile){
                if(((Projectile) e.getDamager()).getShooter() instanceof Player){
                    e.getEntity().setMetadata("lastPlayerDamager_core",new FixedMetadataValue(PixelCore.getI(),((Player) ((Projectile) e.getDamager()).getShooter()).getUniqueId()));
                    e.getEntity().setMetadata("lastPlayerDamagerCause_core",new FixedMetadataValue(PixelCore.getI(),e.getCause().name()));
                }
            }
        }
    }
}
