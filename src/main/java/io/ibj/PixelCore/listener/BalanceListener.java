package io.ibj.PixelCore.listener;

import com.earth2me.essentials.Essentials;
import io.ibj.PixelCore.BigDecimalUtils;
import io.ibj.PixelCore.PixelCore;
import io.ibj.PixelCore.player.APlayer;
import io.ibj.PixelCore.player.ActionBarManager;
import net.ess3.api.events.UserBalanceUpdateEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author joe 2/25/2015
 */
public class BalanceListener implements Listener {

    private Map<UUID, ActionBarManager.ActionBarPane> actionPaneMap = new HashMap<>();

    @EventHandler(priority = EventPriority.MONITOR)
    public void onJoin(PlayerJoinEvent e){
        String format = PixelCore.getI().getCachedConfig().getBalanceFormat();
        BigDecimal balance = Essentials.getPlugin(Essentials.class).getUser(e.getPlayer()).getMoney();
        int balLength = bigDecimalLength(balance);
        format = format.replaceAll("<bal>",getBigDecimal(balance,balLength)).replaceAll("<s_bal>",getShortBigDecimal(balance,balLength));
        APlayer player = PixelCore.getI().getPlayerManager().getPlayer(e.getPlayer());
        if(player == null){
            return;
        }
        ActionBarManager actionBarManager = player.getActionBarManager();
        if(actionBarManager == null){
            return;
        }
        ActionBarManager.ActionBarPane pane = actionBarManager.addPane(format);
        actionPaneMap.put(e.getPlayer().getUniqueId(),pane);
    }

    @EventHandler
    public void onBal(UserBalanceUpdateEvent e){
        ActionBarManager.ActionBarPane pane = actionPaneMap.get(e.getPlayer().getUniqueId());
        if(pane == null){
            return;
        }

        updateBal(e.getNewBalance(),pane);
        interruptPane(e.getOldBalance(),e.getNewBalance().subtract(e.getOldBalance()),pane);
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent e){
        actionPaneMap.remove(e.getPlayer().getUniqueId());
    }

    @EventHandler
    public void onLeave(PlayerKickEvent e){
        actionPaneMap.remove(e.getPlayer().getUniqueId());
    }

    private void updateBal(BigDecimal bal, ActionBarManager.ActionBarPane pane){
        String ret = PixelCore.getI().getCachedConfig().getBalanceFormat();
        int numberLength = bigDecimalLength(bal);

        ret = ret.replaceAll("<bal>",getBigDecimal(bal,numberLength));

        String shortBal = getShortBigDecimal(bal, numberLength);
        ret = ret.replaceAll("<s_bal>",shortBal);
        pane.setPane(ret);
    }

    private int bigDecimalLength(BigDecimal bal) {
        if(bal.signum() == 0){
            return 1;
        }
        double v = Math.floor(BigDecimalUtils.ln(bal, bal.scale()).doubleValue() / Math.log(10)) + 1;
        if(v < 0){
            return 0;
        }
        return (int) v;
    }

    private void interruptPane(BigDecimal bal, BigDecimal delta, ActionBarManager.ActionBarPane pane){
        String ret;
        if(delta.signum() == -1){
            ret = PixelCore.getI().getCachedConfig().getNegativeBalFormat();
            delta = delta.negate();
        }
        else if(delta.signum() == 1)
        {
            ret = PixelCore.getI().getCachedConfig().getPositiveBalFormat();
        }
        else
        {
            return;
        }
        int balLength = bigDecimalLength(bal);
        int deltaLength = bigDecimalLength(delta);
        ret = ret.replaceAll("<bal>",getBigDecimal(bal,balLength)).replaceAll("<s_bal>",getShortBigDecimal(bal,balLength));
        ret = ret.replaceAll("<delta_bal>",getBigDecimal(delta,deltaLength)).replaceAll("<delta_s_bal>",getShortBigDecimal(delta,deltaLength));
        pane.getManager().interruptBar(ret);
    }

    private String getBigDecimal(BigDecimal bal, int length){
        return bal.round(new MathContext(length+2)).toString();
    }

    private String getShortBigDecimal(BigDecimal bal, int numberLength) {
        String shortBal;
        if(numberLength < 4){
            shortBal = bal.round(new MathContext(3)).toPlainString();
        }
        else if(numberLength < 7){
            shortBal = bal.divide(new BigDecimal(1000),new MathContext(3)).toPlainString()+"K";
        }
        else if(numberLength < 10){
            shortBal = bal.divide(new BigDecimal(1000000),new MathContext(3)).toPlainString()+"M";
        }
        else if(numberLength < 13){
            shortBal = bal.divide(new BigDecimal(1000000000),new MathContext(3)).toPlainString()+"B";
        }
        else
        {
            shortBal = bal.divide(new BigDecimal(1000000000000L), new MathContext(3)).toPlainString()+"T";
        }
        return shortBal;
    }

}
