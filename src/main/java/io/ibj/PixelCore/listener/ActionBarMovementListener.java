package io.ibj.PixelCore.listener;

import io.ibj.PixelCore.PixelCore;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 * @author joe 2/26/2015
 */
public class ActionBarMovementListener implements Listener {

    @EventHandler
    public void onMove(PlayerMoveEvent e){
        PixelCore.getI().getPlayerManager().getPlayer(e.getPlayer()).getActionBarManager().onMove();
    }

}
