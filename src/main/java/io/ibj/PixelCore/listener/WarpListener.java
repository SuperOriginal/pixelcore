package io.ibj.PixelCore.listener;

import io.ibj.JLib.format.Format;
import io.ibj.JLib.utils.StringUtils;
import io.ibj.PixelCore.CachedConfig;
import io.ibj.PixelCore.PixelCore;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author joe 2/28/2015
 */
public class WarpListener implements Listener {

    public void calculateAcceptedWarpCommands(){
        Command command = Bukkit.getPluginCommand("warp");
        acceptedWarpCommands = new ArrayList<>(command.getAliases().size()+1);
        acceptedWarpCommands.add(command.getName());
        acceptedWarpCommands.addAll(command.getAliases());
    }

    List<String> acceptedWarpCommands;

    @EventHandler
    public void onPlayerWarpHandle(PlayerCommandPreprocessEvent e){
        String[] command = e.getMessage().split(" ");
        if(command.length == 1) { //Only command
            if(!acceptedWarpCommands.contains(command[0].substring(1).toLowerCase())){
                return;
            }
        }
        else
        {
            return;
        }

        String[] warpsFormat = PixelCore.getI().getFormatList("warp.format");

        String warpReplacement = PixelCore.getI().getFormatRaw("warp.instance");
        String seperator = PixelCore.getI().getFormatRaw("warp.separator");

        for(CachedConfig.WarpEntry entry : PixelCore.getI().getCachedConfig().getWarpEntries()){
            String replacement;
            if (e.getPlayer().hasPermission("core.warp." + entry.getName())) {
                List<String> instances = new LinkedList<>();
                for(String s : entry.getWarps()){
                    if(e.getPlayer().hasPermission("essentials.warps."+s)){
                        instances.add(warpReplacement.replaceAll("<warp>",s));
                    }
                }
                replacement = StringUtils.joinList(seperator,instances);
            }
            else
            {
                replacement = entry.getError();
            }
            replace(warpsFormat,"<"+entry.getName()+">",replacement);
        }
        Format f = new Format(warpsFormat);
        f.sendTo(e.getPlayer());
        e.setCancelled(true);
    }

    private void replace(String[] source, String key, String replaceWith){
        for(int i = 0; i<source.length; i++){
            source[i] = source[i].replaceAll(key,replaceWith);
        }
    }
}
