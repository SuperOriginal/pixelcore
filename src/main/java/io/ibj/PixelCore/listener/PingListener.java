package io.ibj.PixelCore.listener;

import io.ibj.PixelCore.PixelCore;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

/**
 * @author joe 1/22/2015
 */
public class PingListener implements Listener {
    
    @EventHandler
    public void onPing(ServerListPingEvent e){
        if(PixelCore.getI().getCachedConfig().getServerIcon() != null) {
            e.setServerIcon(PixelCore.getI().getCachedConfig().getServerIcon());
        }
        e.setMotd(PixelCore.getI().getCachedConfig().getLine1()+"\n"+PixelCore.getI().getCachedConfig().getLine2());
    }
}
