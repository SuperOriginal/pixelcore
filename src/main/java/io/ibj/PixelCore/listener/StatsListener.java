package io.ibj.PixelCore.listener;

import com.vexsoftware.votifier.model.VotifierEvent;
import io.ibj.JLib.ThreadLevel;
import io.ibj.PixelCore.PixelCore;
import io.ibj.PixelCore.player.APlayer;
import io.ibj.PixelCore.player.Stats;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;

/**
 * @author joe 1/24/2015
 */
public class StatsListener implements Listener {
    
    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent e){
        APlayer player = PixelCore.getI().getPlayerManager().getPlayer(e.getEntity());
        player.getAttribute(Stats.class).incDeaths();
        EntityDamageEvent lastDamageCause = e.getEntity().getLastDamageCause();
        if(lastDamageCause != null) {
            Entity lastDamageEntity = lastDamageCause.getEntity();
            if (lastDamageEntity != null) {
                if (lastDamageEntity instanceof Player) {
                    PixelCore.getI().getPlayerManager().getPlayer((Player) lastDamageEntity).getAttribute(Stats.class).incKills();
                } else if (lastDamageEntity instanceof Projectile) {
                    if (((Projectile) lastDamageEntity).getShooter() instanceof Player) {
                        PixelCore.getI().getPlayerManager().getPlayer((Player) ((Projectile) lastDamageEntity).getShooter()).getAttribute(Stats.class).incKills();
                    }
                }
            }
        }
    }
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void onBreakBlock(BlockBreakEvent e){
        if(e.isCancelled()) return;
        PixelCore.getI().getPlayerManager().getPlayer(e.getPlayer()).getAttribute(Stats.class).incBlocksBroken();
    }
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void onBlockPlace(BlockPlaceEvent e){
        if(e.isCancelled()) return;
        PixelCore.getI().getPlayerManager().getPlayer(e.getPlayer()).getAttribute(Stats.class).incBlocksPlaced();
    }
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void onArrowShoot(ProjectileLaunchEvent e){
        if(e.isCancelled()) return;
        if(e.getEntity() instanceof Arrow){
            if(e.getEntity().getShooter() instanceof Player){
                PixelCore.getI().getPlayerManager().getPlayer((Player) e.getEntity().getShooter()).getAttribute(Stats.class).incArrowsShot();
            }
        }
    }
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void onEntityKilled(EntityDeathEvent e){
        if(!(e.getEntity() instanceof Player)) { //Covered in PlayerDeathEvent
            if(e.getEntity().getLastDamageCause() instanceof Player){
                PixelCore.getI().getPlayerManager().getPlayer((Player) e.getEntity().getLastDamageCause().getEntity()).getAttribute(Stats.class).incMobsKilled();
            }
        }
    }
    
    @EventHandler
    public void onVote(final VotifierEvent e){
        PixelCore.getI().runNow(new Runnable() {
            @Override
            public void run() {
                APlayer player = PixelCore.getI().getPlayerManager().getPlayer(e.getVote().getUsername());
                if(player != null){
                    player.getAttribute(Stats.class).incVotes();
                    PixelCore.getI().getStatsManager().saveAPlayer(player);
                }
            }
        }, ThreadLevel.ASYNC);
        
    }
    
}
