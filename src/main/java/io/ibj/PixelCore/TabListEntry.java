package io.ibj.PixelCore;

import org.bukkit.entity.Player;
import org.bukkit.permissions.Permissible;

import java.util.Objects;

/**
 * @author joe 1/22/2015
 */
public class TabListEntry {
    
    public TabListEntry(String perm, String prefix, String suffix, Integer weight){
        this.perm = perm;
        this.prefix = prefix;
        this.suffix = suffix;
        this.weight = weight;
    }
    
    String perm;
    String prefix;
    String suffix;
    Integer weight;
    
    public boolean hasPerm(Permissible permissible){
        return perm == null || Objects.equals(perm, "") || permissible.hasPermission(perm);
    }
    
    public void apply(Player player){
        player.setPlayerListName(prefix+player.getName()+suffix);
    }
    
    public Integer getWeight(){
        return weight;
    }
    
}
