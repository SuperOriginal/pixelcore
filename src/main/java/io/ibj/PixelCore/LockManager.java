package io.ibj.PixelCore;

/**
 * @author joe 1/24/2015
 */
public class LockManager {
    
    boolean startupLock = true;
    boolean restartLock = false;
    
    public boolean isLocked(){
        return startupLock || restartLock;
    }
    
    public boolean isStartupLocked(){
        return startupLock;
    }
    
    public boolean isRestartLocked(){
        return restartLock;
    }
    
    public void setStartupLock(boolean lock){
        startupLock = lock;
    }
    
    public void setRestartLock(boolean lock){
        restartLock = lock;
    }
}
