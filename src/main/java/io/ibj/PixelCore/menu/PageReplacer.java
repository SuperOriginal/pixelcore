package io.ibj.PixelCore.menu;

import io.ibj.JLib.gui.BasicPage;
import io.ibj.JLib.gui.Button;
import io.ibj.JLib.utils.ItemMetaFactory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

/**
 * @author joe 3/14/2015
 */
public class PageReplacer {

    public PageReplacer(BasicPage page){
        this.page = page;
    }

    BasicPage page;

    public void replace(String key, Object value){
        for(Button b : page.getButtons()){
            if(b == null){
                continue;
            }
            ItemStack stack = b.getIcon();
            if(stack == null){
                continue;
            }
            if(stack.hasItemMeta()){
                ItemMetaFactory factory = new ItemMetaFactory(stack);
                String displayName = stack.getItemMeta().getDisplayName();
                if(displayName != null){
                    factory.setDisplayName(displayName.replaceAll(key,value.toString()));
                }
                if(stack.getItemMeta().hasLore()){
                    List<String> lore = new ArrayList<>(stack.getItemMeta().getLore().size());
                    for(String s : stack.getItemMeta().getLore()){
                        lore.add(s.replaceAll(key,value.toString()));
                    }
                    factory.setLore(lore);
                }
                factory.set();
                b.setIcon(stack);
            }
        }
    }
}
