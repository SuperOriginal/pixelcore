package io.ibj.PixelCore.menu;

import io.ibj.JLib.gui.BasicPage;
import io.ibj.JLib.gui.PageHolder;
import io.ibj.PixelCore.PixelCore;
import org.bukkit.entity.Player;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.InventoryView;

/**
 * @author joe 3/14/2015
 * Im a menu.
 */
public class Menu {

    public Menu(BasicPage basicPage, MenuRegistrar registrar){
        this.rootPage = basicPage;
        this.registrar = registrar;
    }

    BasicPage rootPage;
    MenuRegistrar registrar;

    public void showToPlayer(Player player){
        BasicPage clonedPage;
        try {
            clonedPage = (BasicPage) rootPage.clone();
        } catch (CloneNotSupportedException e) {
            return;
        }
        registrar.applyReplacements(clonedPage,player);
        InventoryView currentView = player.getOpenInventory();
        if(currentView != null){
            InventoryHolder holder = currentView.getTopInventory().getHolder();
            if (holder instanceof PageHolder) {
                PageHolder pageHolder = (PageHolder) holder;
                pageHolder.load(clonedPage);
            }
            else
            {
                PageHolder newHolder = new PageHolder(player,PixelCore.getI());
                newHolder.load(clonedPage);
                newHolder.open();
            }
        }
        else
        {
            PageHolder pageHolder = new PageHolder(player, PixelCore.getI());
            pageHolder.load(clonedPage);
            pageHolder.open();
        }
    }

}
