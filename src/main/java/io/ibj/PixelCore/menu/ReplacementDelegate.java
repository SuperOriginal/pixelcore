package io.ibj.PixelCore.menu;

import org.bukkit.entity.Player;

/**
 * @author joe 3/14/2015
 */
public interface ReplacementDelegate {
    public void replace(PageReplacer delegate, Player player);
}
