package io.ibj.PixelCore.menu;

import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.JLib.file.ResourceFile;
import io.ibj.JLib.file.ResourceReloadHook;
import io.ibj.JLib.file.YAMLFile;
import io.ibj.JLib.gui.*;
import io.ibj.JLib.utils.Colors;
import io.ibj.JLib.utils.ItemMetaFactory;
import io.ibj.JLib.utils.StringUtils;
import io.ibj.PixelCore.PixelCore;
import io.ibj.PixelCore.player.Stats;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import java.util.*;

/**
 * @author joe 3/14/2015
 */
public class MenuRegistrar implements ResourceReloadHook {

    private Map<String, Menu> menuMap = new HashMap<>();

    private Set<ReplacementDelegate> replacementDelegateSet = new HashSet<>();

    public Menu getMenu(String key){
        return menuMap.get(key);
    }

    public Menu getMenu(String... key){
        return getMenu(StringUtils.joinList(" ",key));
    }

    public void registerMenu(Menu menu, String key){
        if(menuMap.containsKey(key)){
            throw new IllegalStateException("Already a menu registered by "+key);
        }
        menuMap.put(key,menu);
    }

    public void registerMenu(Menu menu, String... key){
        registerMenu(menu, StringUtils.joinList(" ", key));
    }

    public void applyReplacements(BasicPage menu, Player player){
        PageReplacer replacer = new PageReplacer(menu);
        for(ReplacementDelegate delegate : replacementDelegateSet){
            delegate.replace(replacer,player);
        }
    }

    public void addReplacementDelegate(ReplacementDelegate delegate){
        replacementDelegateSet.add(delegate);
    }

    @Override
    public void onReload(ResourceFile file) {
        FileConfiguration configuration = ((YAMLFile) file).getConfig();
        menuMap.clear();
        for(String key : configuration.getKeys(false)){
            Menu menu = generateMenu(configuration.getConfigurationSection(key));
            registerMenu(menu,key);
        }
    }

    private Menu generateMenu(ConfigurationSection section){
        String title = Colors.colorify(section.getString("title"));
        ConfigurationSection items = section.getConfigurationSection("items");
        int rows = section.getInt("rows");
        Button[] buttons = new Button[rows*9];
        for(int i = 0; i<rows*9; i++){
            buttons[i] = new Button(null,null);
        }
        for(String key : items.getKeys(false)){
            int placement;
            try{
                placement = Integer.valueOf(key);
            }
            catch(NumberFormatException e){
                continue;
            }

            ConfigurationSection item = items.getConfigurationSection(key);
             Material mat = Material.getMaterial(item.getString("material").toUpperCase());
            if(mat == null) continue;

            ItemStack stack = new ItemStack(mat,item.getInt("amount"), (byte)item.getInt("data",0));
            ItemMetaFactory factory = ItemMetaFactory.create(stack);
            for(String line : item.getStringList("lore")){
                factory.addToLore(Colors.colorify(line));
            }
            factory.addToLore("");
            Integer cost = item.getInt("cost");
            if(cost > 0)
                factory.addToLore(ChatColor.GRAY+"Cost: "+ChatColor.YELLOW+cost);
            factory.setDisplayName(Colors.colorify(item.getString("name")));
            factory.set();
            TokenGroupClickHandler groupClickHandler = new TokenGroupClickHandler(cost);
            for(final String s : item.getStringList("commands")){
                final String lowerLine = s.toLowerCase();
                if(lowerLine.startsWith("cmd:")){
                    groupClickHandler.add(new ClickHandler() {
                        String cmd = s.substring(4,s.length());
                        @Override
                        public void handleClick(Player player, Page page, PageHolder holder, ClickType clickType) throws PlayerException {
                            Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),cmd.replaceAll("<name>",player.getName()));
                        }
                    });
                }
                else if(lowerLine.startsWith("msg:")){
                    groupClickHandler.add(new ClickHandler() {
                        String msg = Colors.colorify(s.substring(4, s.length()));

                        @Override
                        public void handleClick(Player player, Page page, PageHolder holder, ClickType clickType) throws PlayerException {
                            player.sendMessage(msg.replaceAll("<name>", player.getName()));
                        }
                    });
                }
                else if(lowerLine.startsWith("bc:")){
                    groupClickHandler.add(new ClickHandler() {
                        String bc = Colors.colorify(s.substring(3,s.length()));
                        @Override
                        public void handleClick(Player player, Page page, PageHolder holder, ClickType clickType) throws PlayerException {
                            Bukkit.broadcastMessage(bc.replaceAll("<name>",player.getName()));
                        }
                    });
                }
                else if(lowerLine.startsWith("menu:")){
                    if(lowerLine.equalsIgnoreCase("menu:close")){
                        groupClickHandler.add(new ClickHandler() {
                            @Override
                            public void handleClick(Player player, Page page, PageHolder holder, ClickType clickType) throws PlayerException {
                                holder.close();
                            }
                        });
                    }
                    else
                    {
                        groupClickHandler.add(new ClickHandler() {
                            String menu = s.substring(5, s.length());
                            @Override
                            public void handleClick(Player player, Page page, PageHolder holder, ClickType clickType) throws PlayerException {
                                Menu nextMenu = getMenu(menu);
                                if(nextMenu == null){
                                    return;
                                }
                                nextMenu.showToPlayer(player);
                            }
                        });
                    }
                }
            }
            Button button = new Button(stack,groupClickHandler);
            buttons[placement] = button;
        }
        return new Menu(new BasicPage(title,rows*9,buttons),this);
    }

    private class TokenGroupClickHandler extends ArrayList<ClickHandler> implements ClickHandler{
        public TokenGroupClickHandler(int cost){
            this.tokensCost = cost;
        }
        int tokensCost;
        @Override
        public void handleClick(Player player, Page page, PageHolder holder, ClickType clickType) throws PlayerException {
            Stats s = PixelCore.getI().getPlayerManager().getPlayer(player).getAttribute(Stats.class);
            if(s.getTokens() < tokensCost){
                throw new PlayerException(PixelCore.getI().getF("notEnoughTokens"));
            }
            s.withdrawTokens(tokensCost);
            for(ClickHandler handler : this){
                handler.handleClick(player,page,holder,clickType);
            }
        }
    }
}
