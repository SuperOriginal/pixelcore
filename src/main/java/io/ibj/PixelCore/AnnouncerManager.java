package io.ibj.PixelCore;

import com.google.common.collect.ImmutableList;
import io.ibj.JLib.ThreadLevel;
import io.ibj.JLib.file.ResourceFile;
import io.ibj.JLib.file.ResourceReloadHook;
import io.ibj.JLib.file.YAMLFile;
import io.ibj.JLib.format.Format;
import io.ibj.JLib.utils.Colors;
import lombok.AccessLevel;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author joe 2/20/2015
 */
public class AnnouncerManager implements ResourceReloadHook, Runnable {
    @Getter(AccessLevel.PUBLIC)
    boolean enabled;
    List<String> messages = new LinkedList<>();
    @Getter(AccessLevel.PUBLIC)
    int interval = 60;
    int currentPointer = 0;
    int currentTime = 0;

    private YAMLFile fileCache;

    public List<String> getMessages(){
        return ImmutableList.copyOf(messages);
    }

    @Override
    public void onReload(ResourceFile resourceFile) {
        fileCache = ((YAMLFile) resourceFile);
        FileConfiguration config = fileCache.getConfig();
        enabled = config.getBoolean("enabled");
        interval = config.getInt("interval");
        messages = new ArrayList<>();
        for(String s : config.getStringList("messages")){
            messages.add(Colors.colorify(s));
        }
        currentPointer = 0;
    }
    
    public void save(){
        PixelCore.getI().runNow(new Runnable() {
            @Override
            public void run() {
                FileConfiguration config = fileCache.getConfig();
                config.set("enabled",enabled);
                config.set("interval",interval);
                List<String> configMessages = new ArrayList<>(messages.size());
                for(String s : messages){
                    configMessages.add(Colors.decolorify(s));
                }
                config.set("messages",configMessages);
                fileCache.saveConfig();
            }
        }, ThreadLevel.ASYNC);
    }
    
    public void addAnnouncement(String announcement){
        messages.add(Colors.colorify(announcement));
        save();
    }

    public void removeAnnouncement(String a){
        messages.remove(a);
        if(messages.size() >= currentPointer){
            currentPointer = 0;
        }
        save();
    }

    public String removeAnnounement(int index){
        String ret = messages.remove(index);
        if(messages.size() >= currentPointer){
            currentPointer = 0; //Even if size  == 0
        }
        save();
        return ret;
    }

    public void setInterval(int seconds){
        this.interval = seconds;
        save();
    }

    public void enable(boolean enable){
        this.enabled = enable;
        save();
    }

    public void tick(){
        if(!enabled){
            return;
        }
        if(messages.size() == 0){
            return;
        }
        currentTime++;
        if(currentTime >= interval){
            currentPointer++;
            if(currentPointer >= messages.size()){
                currentPointer = 0;
            }
            currentTime = 0;
            Format format = new Format(messages.get(currentPointer));
            format.sendTo(Bukkit.getOnlinePlayers().toArray(new Player[Bukkit.getOnlinePlayers().size()]));
        }
    }

    @Override
    public void run() {
        tick();
    }

    public int getMessageCount(){
        return messages.size();
    }
}
