package io.ibj.PixelCore.player;

import lombok.AllArgsConstructor;
import lombok.Value;

/**
 * @author joe 1/17/2015
 */
@Value
@AllArgsConstructor
public class AttributeTableIdColumn {
    private String table;
    private String column;
}
