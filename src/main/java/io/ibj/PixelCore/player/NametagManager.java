package io.ibj.PixelCore.player;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

/**
 * @author joe 2/21/2015
 */
public class NametagManager {
    
    protected NametagManager(Player player){
        this.playerId = player.getUniqueId();
    }
    
    UUID playerId;
    
    private String defaultPrefix;
    private String defaultSuffix;

    private Map<UUID, String> prefixOverrides = new HashMap<>();
    private Map<UUID, String> suffixOverrides = new HashMap<>();

    private static final String TEAM_PREFIX = "nameTag_";
    
    public void setPrefix(String prefix, Player player){
        prefixOverrides.put(player.getUniqueId(),prefix);
        reapplyTeams(player);
    }
    
    public void setSuffix(String suffix, Player player){
        suffixOverrides.put(player.getUniqueId(),suffix);
        reapplyTeams(player);
    }
    
    public String getPrefix(Player player){
        String prefix = prefixOverrides.get(player.getUniqueId());
        if(prefix != null){
            return prefix;
        }
        return defaultPrefix;
    }
    
    public String getSuffix(Player player){
        String suffix = suffixOverrides.get(player.getUniqueId());
        if(suffix != null){
            return suffix;
        }
        return defaultSuffix;
    }
    
    public void setPrefix(String prefix){
        defaultPrefix = prefix;
        reapplyAllTeams();
    }
    
    public void setSuffix(String suffix){
        defaultSuffix = suffix;
        reapplyAllTeams();
    }
    
    public void resetPrefix(Player player){
        prefixOverrides.remove(player.getUniqueId());
        reapplyTeams(player);
    }
    
    public void resetSuffix(Player player){
        suffixOverrides.remove(player.getUniqueId());
        reapplyTeams(player);
    }

    public void reapplyTeams(Player player){
        applyPrefixSuffix(player,getPrefix(player), getSuffix(player));
    }
    
    private void reapplyAllTeams(){
        for(Player player : Bukkit.getOnlinePlayers()){
            reapplyTeams(player);
        }
    }
    
    private void applyPrefixSuffix(Player player, String prefix, String suffix){
        Scoreboard scoreboard = player.getScoreboard();
        Player me = Bukkit.getPlayer(playerId);
        if(me == null){
            return; //Nothing to return
        }
        if(scoreboard == null){
            scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
            player.setScoreboard(scoreboard);
        }
        Iterator<Team> teamIterator = scoreboard.getTeams().iterator();
        while(teamIterator.hasNext()){     //Unregister previous teams that the player is registered to that do not conform!
            Team team = teamIterator.next();
            if(team.getName().startsWith(TEAM_PREFIX)){
                if(!team.getPrefix().equals(prefix) && !team.getSuffix().equals(suffix)) {
                    team.removePlayer(me);
                    if (team.getSize() == 0) {
                        team.unregister();
                    }
                }
            }
        }
        
        //Now lets reassign with new teams
        if(prefix != null) {
            Team prefixAssignedTeam = getTeamByPrefix(scoreboard, prefix);
            prefixAssignedTeam.addPlayer(me);
        }
        
        if(suffix != null){
            Team suffixAssignableTeam = getTeamBySuffix(scoreboard,suffix);
            suffixAssignableTeam.addPlayer(me);
        }
    }

    private Team getTeamByPrefix(Scoreboard scoreboard, String prefix){
        for(Team team : scoreboard.getTeams()){
            if(team.getPrefix().equals(prefix)){
                return team;
            }
        }
        Team replacementTeam = createNewTeam(scoreboard);

        replacementTeam.setPrefix(prefix);
        return replacementTeam;
    }
    
    private Team getTeamBySuffix(Scoreboard scoreboard, String suffix){
        for(Team team : scoreboard.getTeams()){
            if(team.getSuffix().equals(suffix)){
                return team;
            }
        }
        Team replacementTeam = createNewTeam(scoreboard);
        replacementTeam.setSuffix(suffix);
        return replacementTeam;
        
    }
    
    private Team createNewTeam(Scoreboard scoreboard){
        int i = 0;
        while(true){
            if(scoreboard.getTeam(TEAM_PREFIX+i) == null){
                Team team = scoreboard.registerNewTeam(TEAM_PREFIX + i);
                team.setAllowFriendlyFire(true);
                team.setCanSeeFriendlyInvisibles(false);
                return team;
            }
            i++;
        }
    }
    
    public void setPlayerListName(String playerListName){
        Player ref = Bukkit.getPlayer(playerId);
        if(ref != null){
            ref.setPlayerListName(playerListName);
        }
    }
    
    public String getPlayerListName(){
        Player ref = Bukkit.getPlayer(playerId);
        if(ref == null){
            throw new IllegalStateException("Player has already logged off, and player instance has been disposed.");
        }
        return ref.getPlayerListName();        
    }
}
