package io.ibj.PixelCore.player;

import com.google.common.collect.ImmutableSet;
import io.ibj.JLib.utils.UUIDUtils;
import io.ibj.PixelCore.PixelCore;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.sql.*;
import java.util.*;

/**
 * @author joe 1/17/2015
 */
public class SqlPlayerManager implements PlayerManager {
    
    
    Map<UUID, APlayer> playerMap = new HashMap<>();
    Map<String, APlayer> nameMap = new HashMap<>();
    
    @Override
    public void onJoin(UUID playerId, String playerName, String ip){
        APlayer inRecord = getOfflinePlayer(playerId);
        if(!containedInDB(playerId)){
            try(Connection connection = PixelCore.getI().getDBConnection()) {
                PreparedStatement statement = connection.prepareStatement("insert into player_stats (playerId, timestamp, ip) VALUES (?,?,?);");
                statement.setBytes(1, UUIDUtils.toBytes(playerId));
                statement.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
                statement.setString(3,ip);
                statement.execute();
                statement.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        if(inRecord == null){
            try(Connection connection = PixelCore.getI().getDBConnection()) {
                PreparedStatement statement = connection.prepareStatement("insert into player_main (playerId, realName) VALUES (?,?);");
                statement.setBytes(1, UUIDUtils.toBytes(playerId));
                statement.setString(2, playerName);
                statement.execute();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            Map<Class<? extends Attribute>, Attribute> attributeMap = new HashMap<>();
            inRecord = new SqlAPlayer(playerId,playerName.toLowerCase(),attributeMap,new Timestamp(System.currentTimeMillis()),ip);
            for (AttributeManager<? extends Attribute> attributeManager : PixelCore.getI().getAttributeRegistrar().getManagers()) {
                try {
                    attributeMap.put(attributeManager.getAttributeClass(),attributeManager.generateDefault(inRecord));
                } catch (SQLException e) {
                    attributeManager.getPlugin().handleError(e);
                }
            }
        }
        if(!Objects.equals(inRecord.getName(), playerName)){
            ((SqlAPlayer) inRecord).offlineName = playerName;
            try(Connection connection = PixelCore.getI().getDBConnection()) {
                PreparedStatement statement = connection.prepareStatement("update player_main SET realName = ? where playerId = ?;");
                statement.setString(1,playerName);
                statement.setBytes(2,UUIDUtils.toBytes(playerId));
                statement.execute();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        ((SqlAPlayer)inRecord).played = new Timestamp(System.currentTimeMillis());
        ((SqlAPlayer)inRecord).ip = ip;
        playerMap.put(playerId,inRecord);
        nameMap.put(playerName.toLowerCase(),inRecord);
        PixelCore.getI().getLogger().info("Loaded player "+playerName);
    }
    
    @Override
    public void onLeave(Player player){
        playerMap.remove(player.getUniqueId());
        nameMap.remove(player.getName().toLowerCase());
    }
    
    @Override
    public APlayer getPlayer(Player player){ //They are online!
        APlayer aPlayer = playerMap.get(player.getUniqueId());
        if(aPlayer == null){
            if(player.isOnline()){
                aPlayer = getOfflinePlayer(player);
                playerMap.put(player.getUniqueId(), aPlayer);
            }
        }
        return aPlayer;
    }
    
    @Override
    public APlayer getPlayer(UUID id){
        return playerMap.get(id);
    }
    
    @Override
    public APlayer getPlayer(String name){
        return nameMap.get(name.toLowerCase());
    }
    
    @Override
    public APlayer getOfflinePlayer(UUID id){
        APlayer ret = getPlayer(id);
        if(ret == null){
            try(Connection conn = PixelCore.getI().getDBConnection()) {
                PreparedStatement statement = conn.prepareStatement(getUUIDQuery());
                statement.setBytes(1, UUIDUtils.toBytes(id));
                try(ResultSet set = statement.executeQuery()) {
                    if (!set.next()) {
                        return null;
                    }
                    return decodeFromResultSet(set);
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        return ret;
    }
    
    @Override
    public APlayer getOfflinePlayer(String name){
        name = name.toLowerCase();
        APlayer ret = getPlayer(name);
        if(ret == null){
            try(Connection conn = PixelCore.getI().getDBConnection()) {
                PreparedStatement statement = conn.prepareStatement(getNameQuery());
                statement.setString(1,name);
                try(ResultSet set = statement.executeQuery()) {
                    if (!set.next()) {
                        return null;
                    }
                    return decodeFromResultSet(set);
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        return ret;
    }
    
    @Override
    public APlayer getOfflinePlayer(OfflinePlayer offlinePlayer){
        return getOfflinePlayer(offlinePlayer.getUniqueId());
    }

    @Override
    public Set<APlayer> getOnlinePlayers() {
        ImmutableSet.Builder<APlayer> playerBuilder = new ImmutableSet.Builder<>();
        Iterator<APlayer> playerIterator = playerMap.values().iterator();
        while(playerIterator.hasNext()){
            APlayer player = playerIterator.next();
            if(!player.getPlayer().isOnline()){
                playerIterator.remove();
                nameMap.remove(player.getName());
            }
            else{
                playerBuilder.add(player);
            }
        }
        return playerBuilder.build();
    }

    private StringBuilder getQueryJoinHeader(){
        StringBuilder builder = new StringBuilder();
        builder.append("select player_main.*, "); //Initial append, select from initial table
        for (AttributeManager<? extends Attribute> attributeManager : PixelCore.getI().getAttributeRegistrar().getManagers()) {
            for (AttributeTableIdColumn attributeTableIdColumn : attributeManager.getTables()) {
                builder.append(attributeTableIdColumn.getTable()).append(".* ");
            }
        }
        builder.delete(builder.length()-1, builder.length());
        builder.append(" from player_main ");
        for (AttributeManager<? extends Attribute> attributeManager : PixelCore.getI().getAttributeRegistrar().getManagers()) {
            for (AttributeTableIdColumn attributeTableIdColumn : attributeManager.getTables()) {
                builder.append("left join ").append(attributeTableIdColumn.getTable()).append(" on player_main.playerId=").append(attributeTableIdColumn.getTable()).append(".").append(attributeTableIdColumn.getColumn()).append(" ");
            }
        }
        builder.append("where ");
        return builder;
    }
    
    private String getNameQuery(){
        StringBuilder builder = getQueryJoinHeader();
        builder.append("player_main.playerId = ?;");
        return builder.toString();
    }

    private boolean containedInDB(UUID uuid){
        try(Connection con = PixelCore.getI().getDBConnection()){
            PreparedStatement statement = con.prepareStatement("SELECT * FROM player_stats WHERE playerId = ?");
            statement.setBytes(1,UUIDUtils.toBytes(uuid));

            ResultSet set = statement.executeQuery();

            if(!set.next()){
                statement.close();
                return false;
            }else{
                statement.close();
                return true;
            }

        }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }
    
    private String getUUIDQuery(){
        StringBuilder builder = getQueryJoinHeader();
        builder.append("player_main.playerId = ?;");
        String s = builder.toString();
        return s;
    }
    
    private APlayer decodeFromResultSet(ResultSet set) throws SQLException {
        String name = set.getString("realName");
        UUID id = UUIDUtils.fromBytes(set.getBytes("playerId"));
        Timestamp stamp = set.getTimestamp("timestamp");
        String ip = set.getString("ip");
        Map<Class<? extends Attribute>, Attribute> attributeMap = new HashMap<>();
        APlayer ret = new SqlAPlayer(id,name,attributeMap,stamp,ip);
        for (AttributeManager<? extends Attribute> attributeManager : PixelCore.getI().getAttributeRegistrar().getManagers()) {
            set.first();
            attributeMap.put(attributeManager.getAttributeClass(),attributeManager.deserializeAttribute(ret,set));
        }
        return ret;
    }
}
