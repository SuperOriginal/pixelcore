package io.ibj.PixelCore.player;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.Set;
import java.util.UUID;

/**
 * @author joe 1/18/2015
 */
public interface PlayerManager {
    void onJoin(UUID playerId, String playerName, String ip);

    void onLeave(Player player);

    APlayer getPlayer(Player player);

    APlayer getPlayer(UUID id);

    APlayer getPlayer(String name);

    APlayer getOfflinePlayer(UUID id);

    APlayer getOfflinePlayer(String name);

    APlayer getOfflinePlayer(OfflinePlayer offlinePlayer);
    
    Set<APlayer> getOnlinePlayers();
}
