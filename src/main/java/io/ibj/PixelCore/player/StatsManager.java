package io.ibj.PixelCore.player;

import com.google.common.collect.ImmutableSet;
import io.ibj.JLib.JPlug;
import io.ibj.JLib.db.utils.ScriptRunner;
import io.ibj.JLib.utils.UUIDUtils;
import io.ibj.PixelCore.PixelCore;
import org.bukkit.Bukkit;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.*;
import java.util.Set;

/**
 * @author joe 1/24/2015
 */
public class StatsManager implements AttributeManager<Stats>, Runnable {
    @Override
    public JPlug getPlugin() {
        return PixelCore.getI();
    }

    @Override
    public Class<Stats> getAttributeClass() {
        return Stats.class;
    }

    @Override
    public Stats deserializeAttribute(APlayer player, ResultSet set) throws SQLException {
        return new Stats(player,
                set.getInt("kills"),
                set.getInt("deaths"),
                set.getInt("blocksBroken"),
                set.getInt("mobsKilled"),
                set.getLong("playingTime"),
                set.getInt("blocksPlaced"),
                set.getInt("arrowsShot"),
                set.getInt("votes"),
                set.getInt("prestige"),
                set.getInt("tokens"));
    }

    @Override
    public Stats generateDefault(APlayer player) throws SQLException {
        try(Connection conn = PixelCore.getI().getDBConnection()){
            //Ok, lets first try to get the rows from the Tarkan's tables.
            PreparedStatement queryGeneral = conn.prepareStatement("select * from pixel.pp_general where uuid = ?;");
            queryGeneral.setString(1,player.getId().toString());
            ResultSet set = queryGeneral.executeQuery();
            int prestiges = 0;
            if (set.next()) {
                prestiges = set.getInt("prestiges");
            }
            set.close();
            PreparedStatement statement = conn.prepareStatement("update player_stats set prestige = ? where playerId = ?;");
            statement.setBytes(2, UUIDUtils.toBytes(player.getId()));
            statement.setInt(1, prestiges);
            statement.execute();
            return new Stats(player,0,0,0,0,0,0,0,0,prestiges,0);
        }
    }

    @Override
    public void initializeTable() throws SQLException, IOException {
        try(Connection connection = PixelCore.getI().getDBConnection()) {
            ScriptRunner runner = new ScriptRunner(connection, true, true);
            try (Reader reader = new InputStreamReader(PixelCore.getI().getResource("stats.sql"))) {
                runner.runScript(reader);
            }
        }
    }

    @Override
    public Set<AttributeTableIdColumn> getTables() {
        return ImmutableSet.of(new AttributeTableIdColumn("player_stats", "playerId"));
    }

    @Override
    public void run() {
        PixelCore.getI().getLogger().info("Saving stats of all online players....");
        int counter = 0;
        try(Connection conn = PixelCore.getI().getDBConnection()) {
            PreparedStatement statsUpdate = conn.prepareStatement("update player_stats SET kills = ?, deaths = ?, blocksBroken = ?, mobsKilled = ?, playingTime = ?, blocksPlaced = ?, arrowsShot = ?, votes = ?, prestige = ?, tokens = ?, ip = ? where playerId = ?;");
            for (APlayer player : PixelCore.getI().getPlayerManager().getOnlinePlayers()) {
                counter ++;
                Stats s = player.getAttribute(Stats.class);
                statsUpdate.setInt(1,s.getKills());
                statsUpdate.setInt(2,s.getDeaths());
                statsUpdate.setInt(3,s.getBlocksBroken());
                statsUpdate.setInt(4,s.getMobsKilled());
                statsUpdate.setLong(5,s.getPlayingTime());
                statsUpdate.setInt(6,s.getBlocksPlaced());
                statsUpdate.setInt(7,s.getArrowsShot());
                statsUpdate.setInt(8,s.getVotes());
                statsUpdate.setInt(9,s.getPrestige());
                statsUpdate.setInt(10,s.getTokens());
                statsUpdate.setString(11,((SqlAPlayer)player).ip);
                statsUpdate.setBytes(12,UUIDUtils.toBytes(s.getPlayer().getId()));
                statsUpdate.execute();
            }
            PixelCore.getI().getLogger().info("Saved stats of "+counter+" players.");
        } catch (SQLException e) {
            PixelCore.getI().getLogger().info("Error occurred during saving of stats.");
            PixelCore.getI().handleError(e);
        }
    }
    
    public void saveAPlayer(APlayer player){
        if(player == null){
            return;
        }
        PixelCore.getI().getLogger().info("Saving stats of "+player.getName());
        try(Connection conn = PixelCore.getI().getDBConnection()) {

            PreparedStatement statsUpdate = conn.prepareStatement("update player_stats SET kills = ?, deaths = ?, blocksBroken = ?, mobsKilled = ?, playingTime = ?, blocksPlaced = ?, arrowsShot = ?, votes = ?, prestige = ?, tokens = ?, timestamp = ?, ip = ? where playerId = ?;");
            Stats s = player.getAttribute(Stats.class);
            statsUpdate.setInt(1, s.getKills());
            statsUpdate.setInt(2,s.getDeaths());
            statsUpdate.setInt(3,s.getBlocksBroken());
            statsUpdate.setInt(4,s.getMobsKilled());
            statsUpdate.setLong(5,s.getPlayingTime());
            statsUpdate.setInt(6,s.getBlocksPlaced());
            statsUpdate.setInt(7,s.getArrowsShot());
            statsUpdate.setInt(8,s.getVotes());
            statsUpdate.setInt(9,s.getPrestige());
            statsUpdate.setInt(10,s.getTokens());
            statsUpdate.setTimestamp(11,new Timestamp(System.currentTimeMillis()));
            statsUpdate.setString(12,((SqlAPlayer)player).ip);
            statsUpdate.setBytes(13,UUIDUtils.toBytes(s.getPlayer().getId()));
            statsUpdate.execute();
            PixelCore.getI().getLogger().info("Saved stats.");
        } catch (SQLException e) {
            PixelCore.getI().getLogger().info("Error occurred during saving of stats.");
            PixelCore.getI().handleError(e);
        }
    }
}
