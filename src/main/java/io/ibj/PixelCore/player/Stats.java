package io.ibj.PixelCore.player;

import io.ibj.JLib.JPlug;
import io.ibj.PixelCore.PixelCore;
import lombok.Getter;
import lombok.SneakyThrows;

import java.lang.ref.WeakReference;

/**
 * @author joe 1/24/2015
 */
public class Stats implements Attribute {
    
    public Stats(APlayer player, int kills, int deaths, int blocksBroken, int mobsKilled, long playingMs, int blocksPlaced, int arrowsShot, int votes, int prestige, int tokens){
        aPlayerWeakReference = new WeakReference<>(player);
        this.kills = kills;
        this.deaths = deaths;
        this.blocksBroken = blocksBroken;
        this.mobsKilled = mobsKilled;
        this.playingTime = playingMs;
        this.blocksPlaced = blocksPlaced;
        this.arrowsShot = arrowsShot;
        this.votes = votes;
        this.prestige = prestige;
        this.tokens = tokens;
    }
    
    WeakReference<APlayer> aPlayerWeakReference;
    
    @Getter
    int kills;
    @Getter
    int deaths;
    @Getter
    int blocksBroken;
    @Getter
    int mobsKilled;
    @Getter
    long playingTime;
    @Getter
    int blocksPlaced;
    @Getter
    int arrowsShot;
    @Getter
    int votes;
    @Getter
    int prestige;
    @Getter
    int tokens;
    
    @Override
    public APlayer getPlayer() {
        return aPlayerWeakReference.get();
    }

    @Override
    public JPlug getPlugin() {
        return PixelCore.getI();
    }
    
    @SneakyThrows
    public void setKills(int kills){
        this.kills = kills;
    }
    
    @SneakyThrows
    public void incKills(){
        this.kills++;
    }

    @SneakyThrows
    public void setDeaths(int deaths){
        this.deaths = deaths;
    }

    @SneakyThrows
    public void incDeaths(){
        this.deaths++;
    }

    @SneakyThrows
    public void setBlocksBroken(int blocksBroken){
        this.blocksBroken = blocksBroken;
    }

    @SneakyThrows
    public void incBlocksBroken(){
        this.blocksBroken++;
    }
    
    @SneakyThrows
    public void setMobsKilled(int killed){
        mobsKilled = killed;
    }
    
    @SneakyThrows
    public void incMobsKilled(){
        mobsKilled++;
    }
    
    @SneakyThrows
    public void setBlocksPlaced(int blocksPlaced){
        this.blocksPlaced = blocksPlaced;
    }
    
    @SneakyThrows
    public void incBlocksPlaced() {
        this.blocksPlaced++;
    }
    
    @SneakyThrows
    public void setArrowsShot(int arrowsShot){
        this.arrowsShot = arrowsShot;
    }
    
    @SneakyThrows
    public void incArrowsShot(){
        arrowsShot++;
    }

    @SneakyThrows
    public void setVotes(int votes){
        this.votes = votes;
    }
    
    @SneakyThrows
    public void incVotes(){
        votes++;
    }
    
    public void setPrestige(int prestige){
        this.prestige = prestige;
    }
    
    public void incPrestige(){
        this.prestige++;
    }
    
    public void setPlayingTime(final long playingTime){
        this.playingTime = playingTime;
    }

    public void incTokens(){
        tokens++;
    }

    public void setTokens(int tokens){
        this.tokens = tokens;
    }

    public void withdrawTokens(int tokens){
        this.tokens -= tokens;
    }

    public void addTokens(int tokens){
        this.tokens += tokens;
    }
}
