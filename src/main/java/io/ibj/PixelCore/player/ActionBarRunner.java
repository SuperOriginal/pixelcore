package io.ibj.PixelCore.player;

import io.ibj.PixelCore.PixelCore;

/**
 * @author joe 2/26/2015
 */
public class ActionBarRunner implements Runnable {
    @Override
    public void run() {
        for(APlayer player : PixelCore.getI().getPlayerManager().getOnlinePlayers()){
            ActionBarManager actionBarManager = player.getActionBarManager();
            if(actionBarManager != null) {
                actionBarManager.tick();
            }
        }
    }
}
