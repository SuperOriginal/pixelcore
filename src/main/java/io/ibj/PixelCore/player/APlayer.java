package io.ibj.PixelCore.player;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * @author joe 1/17/2015
 * Represents a player within the database that may be online. 
 */
public interface APlayer {
    /**
     * Returns the player, if they are online. If they are not online, the method throws an IllegalStateException. 
     * @return Player which APlayer represents
     */
    Player getOnlinePlayer();

    /**
     * Returns the player this instance represents.
     * @return OfflinePlayer for this instance
     */
    OfflinePlayer getPlayer();

    /**
     * Returns the ID of the given player. This is always constant, and will never change. 
     * @return ID of the player, as defined by Mojang
     */
    UUID getId();

    /**
     * Returns the last known playername for the player. This may change, but is not likely.
     * @return Name of the player
     */
    String getName();

    /**
     * Returns the attribute associated with the given player. If not found, will throw an exception.
     * @param clazz Class of the attribute to return.
     * @param <T> Class parameter of attribute
     * @return Actual attribute of the player given the specified class.
     */
    <T extends Attribute> T getAttribute(Class<T> clazz);

    /**
     * Returns whether the player has the specified attribute
     * @param clazz Class of the attribute to search for
     * @return Whether the player has the attribute
     */
    boolean hasAttribute(Class<? extends Attribute> clazz);

    ActionBarManager getActionBarManager();

    CooldownManager getCooldownManager();

    NametagManager getNametagManager();

    TitleManager getTitleManager();
    
}
