package io.ibj.PixelCore.player;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import io.ibj.JLib.ThreadLevel;
import io.ibj.JLib.TimePeriod;
import io.ibj.JLib.TimeUnit;
import io.ibj.PixelCore.PixelCore;
import io.puharesource.mc.titlemanager.api.TabTitleObject;
import io.puharesource.mc.titlemanager.api.TitleObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Value;
import lombok.experimental.Builder;
import lombok.experimental.Wither;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.lang.ref.WeakReference;

/**
 * @author joe 2/28/2015
 */
public class TitleManager {

    public TitleManager(Player player){
        this.player = new WeakReference<>(player);
    }

    WeakReference<Player> player;

    public void sendPopup(String title, String subtitle) {
        sendPopup(title, subtitle,new PopupContext());
    }

    public void sendPopup(String title, String subtitle, PopupContext context){
        Player ref = player.get();
        if(ref == null){
            return;
        }
        new TitleObject(title,subtitle).setFadeIn(context.getFadeIn()).setFadeOut(context.getFadeOut()).setStay(context.getStay()).send(ref);
    }

    public void sendTabTitle(String header, String footer){
        Player ref = player.get();
        if(ref == null){
            return;
        }
        new TabTitleObject(header,footer).send(ref);
    }

    public void sendTabHeader(String header){
        Player ref = player.get();
        if(ref == null){
            return;
        }
        new TabTitleObject(header, TabTitleObject.Position.HEADER).send(ref);
    }

    public void sendTabFooter(String footer){
        Player ref = player.get();
        if(ref == null){
            return;
        }
        new TabTitleObject(footer,TabTitleObject.Position.FOOTER).send(ref);
    }

    public void sendMovingHologram(String message, Location location){
        sendMovingHologram(new HologramContext(location), message);
    }

    public void sendMovingHologram(final HologramContext context, final String... messages){
        final Player ref = player.get();
        if(ref == null) {
            return;
        }
        PixelCore.getI().runNow(new Runnable() {
            @Override
            public void run() {
                Hologram hologram = HologramsAPI.createHologram(PixelCore.getI(),context.getLocation());
                hologram.getVisibilityManager().setVisibleByDefault(false);
                hologram.getVisibilityManager().showTo(ref);
                for(String message : messages){
                    hologram.appendTextLine(message);
                }
                PixelCore.getI().runLater(new HologramRunnable(hologram,context.getLifetime(),context.getVelocityUp()),ThreadLevel.SYNC,new TimePeriod(1,TimeUnit.TICKS));

            }
        },ThreadLevel.SYNC);


    }

    @Data
    public static class PopupContext{
        public PopupContext(){}
        public PopupContext(int fadeIn, int stay,int fadeOut){
            this.fadeIn = fadeIn;
            this.fadeOut = fadeOut;
            this.stay = stay;
        }
        private int fadeIn = -1;
        private int fadeOut = -1;
        private int stay = -1;
    }

    @Data
    public static class HologramContext{
        public HologramContext(Location location){
            this.location = location;
        }

        public HologramContext(Location location, double velocityUp, int lifetime){
            this.location = location;
            this.velocityUp = velocityUp;
            this.lifetime = lifetime;
        }
        private Location location;
        private double velocityUp = 0.05;
        private int lifetime = 40;
    }

    private static class HologramRunnable implements Runnable{

        public HologramRunnable(Hologram hologram, int lifetime, double velocityUp){
            this.hologram = hologram;
            this.lifetime = lifetime;
            this.velocityUp = velocityUp;
        }

        Hologram hologram;
        int iterations = 0;

        int lifetime;
        double velocityUp;

        @Override
        public void run() {
            if(iterations > lifetime){
                hologram.delete();
                return;
            }
            hologram.teleport(hologram.getLocation().add(0,velocityUp,0));
            iterations++;
            PixelCore.getI().runLater(this, ThreadLevel.SYNC,new TimePeriod(1, TimeUnit.TICKS));
        }
    }
}

