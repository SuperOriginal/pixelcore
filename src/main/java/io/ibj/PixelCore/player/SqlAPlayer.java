package io.ibj.PixelCore.player;

import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.sql.Timestamp;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.UUID;

/**
 * @author joe 1/18/2015
 */
public class SqlAPlayer implements APlayer {
    
    public SqlAPlayer(UUID id, String name, Map<Class<? extends Attribute>,? extends Attribute> attributeMap, Timestamp timestamp, String ip){
        this.id = id;
        this.ip = ip;
        this.offlineName = name;
        this.attributeMap = attributeMap;
        played = timestamp;
        Player player = Bukkit.getPlayer(id);
        cooldownManager = new CooldownManager();
        if(player != null){
            actionBarManager = new ActionBarManager(player);
            nametagManager = new NametagManager(player);
        }
        else
        {
            actionBarManager = null;
            nametagManager = null;
        }
    }
    
    UUID id;
    @Getter
    String ip;
    String offlineName;
    Map<Class<? extends Attribute>, ? extends Attribute> attributeMap;
    ActionBarManager actionBarManager;
    CooldownManager cooldownManager;
    NametagManager nametagManager;
    TitleManager titleManager;
    @Getter
    Timestamp played;

    @Override
    public Player getOnlinePlayer() {
        return Bukkit.getPlayer(id);
    }

    @Override
    public OfflinePlayer getPlayer() {
        return Bukkit.getOfflinePlayer(id);
    }

    @Override
    public UUID getId() {
        return id;
    }

    @Override
    public String getName() {
        return offlineName;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends Attribute> T getAttribute(Class<T> clazz) {
        if(!hasAttribute(clazz)){
            throw new NoSuchElementException("There is no attribute by that class.");
        }
        return (T) attributeMap.get(clazz);
    }

    @Override
    public boolean hasAttribute(Class<? extends Attribute> clazz) {
        return attributeMap.containsKey(clazz);
    }

    @Override
    public ActionBarManager getActionBarManager() {
        if(actionBarManager == null){
            Player player = Bukkit.getPlayer(id);
            if(player != null){
                actionBarManager = new ActionBarManager(player);
            }
        }
        return actionBarManager;
    }

    @Override
    public CooldownManager getCooldownManager() {
        return cooldownManager;
    }

    @Override
    public NametagManager getNametagManager() {
        if(nametagManager == null){
            Player player = Bukkit.getPlayer(id);
            if(player != null){
                nametagManager = new NametagManager(player);
            }
        }
        return nametagManager;
    }

    @Override
    public TitleManager getTitleManager() {
        if(titleManager == null){
            Player player = Bukkit.getPlayer(id);
            if(player != null){
                titleManager = new TitleManager(player);
            }
        }
        return titleManager;
    }
}
