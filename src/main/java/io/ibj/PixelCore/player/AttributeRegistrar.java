package io.ibj.PixelCore.player;

import com.google.common.collect.ImmutableSet;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author joe 1/17/2015
 */
public class AttributeRegistrar {

    Map<Class<? extends Attribute>,AttributeManager<? extends Attribute>> managerMap = new HashMap<>();
    
    public Set<AttributeManager<? extends Attribute>> getManagers(){
        return ImmutableSet.copyOf(managerMap.values());
    }
    
    @SuppressWarnings("unchecked")
    public <T extends Attribute> AttributeManager<T> getManager(Class<T> clazz){
        return (AttributeManager<T>) managerMap.get(clazz);
    }
    
    public void registerAttributeManager(AttributeManager<? extends Attribute> manager){
        if(managerMap.containsValue(manager)){
            throw new IllegalStateException("Attribute Manager already registered.");
        }
        try {
            manager.initializeTable();
        } catch (Exception e){
            manager.getPlugin().handleError(e);
        }
        managerMap.put(manager.getAttributeClass(),manager);
    }
    
    public Map<Class<? extends Attribute>, ?> deseralizeAttributes(APlayer player, ResultSet set){
        Map<Class<? extends Attribute>, Attribute> ret = new HashMap<>();
        for(AttributeManager<? extends Attribute> manager : getManagers()){
            try {
                ret.put(manager.getAttributeClass(),manager.deserializeAttribute(player,set));
            } catch (SQLException e) {
                manager.getPlugin().handleError(e);
            }
        }
        return ret;
    }
    
}
