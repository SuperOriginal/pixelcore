package io.ibj.PixelCore.player;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.lang.ref.WeakReference;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * @author joe 2/21/2015
 */
public class ActionBarManager {

    ActionBarManager(Player player){
        this.playerId = player.getUniqueId();
        backgroundPanes = new ConcurrentLinkedQueue<>();
    }

    UUID playerId;

    Queue<ActionBarPane> backgroundPanes;

    ActionBarPane interruptPane = null;

    private int interruptPaneTimer = 0;
    private int backgroundPaneTimer = 0;

    @Getter @Setter
    private int defaultBackgroundTime = 5;
    @Getter @Setter
    private int defaultInterruptTime = 2;

    private boolean hasMoved = false;

    private boolean shouldShow = true;

    public ActionBarPane getActivePane(){
        if(!isShowing()){
            return null;
        }
        if(interruptPane != null){
            return interruptPane;
        }
        return backgroundPanes.peek();
    }

    public boolean isShowing(){
        return hasMoved && shouldShow && (interruptPane != null || backgroundPanes.size() > 0);
    }

    public void enable(boolean enable){
        this.shouldShow = enable;
        if(!isShowing()){
            sendActionBar("");
        }
    }

    public void onMove(){
        hasMoved = true;
    }

    public ActionBarPane addPane(String display){
        return addPane(display,defaultBackgroundTime);
    }

    public ActionBarPane addPane(String display, int timeToDisplay){
        ActionBarPane pane = new ActionBarPane(this,display,timeToDisplay);
        backgroundPanes.add(pane);
        return pane;
    }

    public ActionBarPane interruptBar(String message, int time){
        interruptPane = new ActionBarPane(this,message,time);
        interruptPaneTimer = 0;
        sendActionBar(message);
        return interruptPane;
    }

    public ActionBarPane interruptBar(String message){
        return interruptBar(message,defaultInterruptTime);
    }

    public void tick(){
        if(!isShowing()){
            sendActionBar("");
            return;
        }
        if(interruptPane != null){
            interruptPaneTimer++;
            if(interruptPane.getTimePeriod() <= interruptPaneTimer){
                interruptPane = null; //Expired
            }
        }
        backgroundPaneTimer++;
        ActionBarPane activeBackgroundPane = backgroundPanes.peek();
        if(activeBackgroundPane != null){
            if(activeBackgroundPane.getTimePeriod() <= backgroundPaneTimer){
                backgroundPanes.add(backgroundPanes.poll()); //Send current to end of queue
                backgroundPaneTimer = 0;
            }

        }
        ActionBarPane current = getActivePane();
        if(current != null){
            sendActionBar(current.getPane());
        }
    }

    private void sendActionBar(String message){
        if(!hasMoved){
            return;//Should not send if they have not moved. This is more for Minechat players, so that their chat does not get spammed.
        }
        Player ref = Bukkit.getPlayer(playerId);
        if(ref == null){
            return;
        }

        new io.puharesource.mc.titlemanager.api.ActionbarTitleObject(message).send(ref);
    }

    /**
     * @author joe 2/21/2015
     * Represents a pane within the actionbar. Can specify what text is shown as well as how long it should be shown.
     */
    public static class ActionBarPane {

        ActionBarPane(ActionBarManager manager, String pane, int timePeriod){
            this.pane = pane;
            this.timePeriod = timePeriod;
            this.manager = manager;
        }

        @Getter
        private String pane;

        public void setPane(String pane){
            this.pane = pane;
            if(manager.getActivePane() == this){
                manager.sendActionBar(pane);
            }
        }

        @Getter
        @Setter
        private int timePeriod;

        @Getter
        private ActionBarManager manager;

        public boolean isRegistered(){
            return manager != null;
        }

        public void deregister(){
            if(manager == null){
                throw new IllegalStateException("Already deregistered!");
            }

            manager.backgroundPanes.remove(this);
            manager = null;
        }

    }
}
