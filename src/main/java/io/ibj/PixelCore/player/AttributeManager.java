package io.ibj.PixelCore.player;

import io.ibj.JLib.JPlug;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Set;

/**
 * @author joe 1/17/2015
 */
public interface AttributeManager<T extends Attribute> {

    /**
     * Returns the plugin that is managing this type of attribute
     * @return  Plugin associated with this attribute
     */
    JPlug getPlugin();

    /**
     * Returns the class of the attribute that this manager manages 
     * @return Attribute class
     */
    Class<T> getAttributeClass();

    /**
     * Returns an attribute from the given player, from the passed resultset. THIS METHOD SHOULD NOT CALL THE NEXT ROW IN THE RESULT SET! Bad things happen.
     * @param player Player that the row is being deserialized for
     * @param set Actual row result set to be deserialized from.
     * @return Deserialized attribute from the player and row
     */
    T deserializeAttribute(APlayer player, ResultSet set) throws SQLException;

    /**
     * Called when a new player joins. Not called if the plugin does not fill in details, or the player outlives plugin.
     * @param player Player to generate attribute for
     * @return Attribute generated
     */
    T generateDefault(APlayer player) throws SQLException;
    /**
     * Called once when registered to initialized the joinable table.
     * @throws java.sql.SQLException Any exception that is to be thrown.
     */
    void initializeTable() throws SQLException, IOException;

    /**
     * Returns a set of all tables and identifying columns for this attribute manager
     * @return Set of table column sets
     */
    Set<AttributeTableIdColumn> getTables();
}
