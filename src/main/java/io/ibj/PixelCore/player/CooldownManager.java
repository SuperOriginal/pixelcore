package io.ibj.PixelCore.player;

import lombok.Getter;
import org.joda.time.Instant;

import java.util.*;

/**
 * @author joe 2/22/2015
 * Manages all cooldowns for a player.
 */

//TODO: Implement persistant
public class CooldownManager {

    private Map<String, CooldownEntry> timeoutMap = new HashMap<>();

    public boolean isKeyOnCooldown(String key){
        CooldownEntry entry = timeoutMap.get(key);
        if(entry == null){
            return false;
        }
        return Instant.now().isBefore(entry.expire);
    }

    public boolean cooldown(String key, Instant expire, boolean persistant, boolean reset){
        if(reset){
            CooldownEntry previousEntry = timeoutMap.put(key,new CooldownEntry(expire,persistant));
            if(previousEntry != null){
                return Instant.now().isBefore(previousEntry.expire);
            }
            return false;
        }
        else
        {
            CooldownEntry currentEntry = timeoutMap.get(key);
            if(currentEntry != null){
                if(Instant.now().isBefore(currentEntry.expire)){
                    return true;
                }
            }
            timeoutMap.put(key,new CooldownEntry(expire,persistant));
            return false;
        }

    }

    public static class CooldownEntry{
        @Getter
        private Instant expire;
        @Getter
        private boolean persistant;
        public CooldownEntry(Instant expire, boolean persistant){
            this.expire = expire;
            this.persistant = persistant;
        }
    }
}
