package io.ibj.PixelCore.player;

import io.ibj.JLib.JPlug;

/**
 * @author joe 1/17/2015
 * Represents any additional information that can be appended to a PlayerRecord
 */
public interface Attribute {

    APlayer getPlayer();
    
    JPlug getPlugin();
    
}
