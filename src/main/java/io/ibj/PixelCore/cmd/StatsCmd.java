package io.ibj.PixelCore.cmd;

import com.swifteh.GAL.GAL;
import com.swifteh.GAL.VoteAPI;
import io.ibj.JLib.ThreadLevel;
import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.ICmd;
import io.ibj.JLib.cmd.annotations.Cmd;
import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.JLib.format.TagStyle;
import io.ibj.PixelCore.PixelCore;
import io.ibj.PixelCore.player.APlayer;
import io.ibj.PixelCore.player.Stats;
import io.ibj.PixelGangs.PixelGangs;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * @author joe 1/24/2015
 */
@Cmd(name = "stats",
    description = "Retrieves the stats for a player.",
    usage = "/stats [player]",
        perm = "core.stats",
    max = 1)
public class StatsCmd implements ICmd {
    @Override
    public boolean execute(CommandSender commandSender, ArgsSet argsSet) throws PlayerException {
        APlayer target;
        if(argsSet.size() == 0){
            if(!(commandSender instanceof Player)){
                throw new PlayerException("Must be a player to execute with no argument!");
            }
            target = PixelCore.getI().getPlayerManager().getPlayer((Player) commandSender);
        }
        else
        {
            target = PixelCore.getI().getPlayerManager().getOfflinePlayer(argsSet.get(0).getAsString());
            if(target == null){
                throw new PlayerException(PixelCore.getI().getF("stats.playerNotFound").replace("<player>",argsSet.get(0).getAsString()));
            }
        }
        Stats stats = target.getAttribute(Stats.class);
        PixelCore.getI().runNow(new Runnable() {
            @Override
            public void run() {
                int kills = PixelGangs.getI().getPlayers().fromName(target.getName()).getKills();
                int deaths = PixelGangs.getI().getPlayers().fromName(target.getName()).getDeaths();
                int votes = VoteAPI.getVoteTotal(target.getName());
                PixelCore.getI().runNow(new Runnable() {
                    @Override
                    public void run() {
                        PixelCore.getI().getF("stats.return").replace("<player>",target.getName())
                                .replace("<kills>", kills)
                                .replace("<deaths>", deaths)
                                .replace("<blocksBroken>", stats.getBlocksBroken())
                                .replace("<mobsKilled>",stats.getMobsKilled())
                                .replace("<blocksPlaced>",stats.getBlocksPlaced())
                                .replace("<arrowsShot>",stats.getArrowsShot())
                                .replace("<votes>", votes).setTagStyle(TagStyle.EVERY_LINE).sendTo(commandSender);
                    }
                },ThreadLevel.SYNC);
            }
        }, ThreadLevel.ASYNC);

        return true;
    }
}
