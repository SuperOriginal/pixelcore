package io.ibj.PixelCore.cmd;

import io.ibj.JLib.ThreadLevel;
import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.ICmd;
import io.ibj.JLib.cmd.annotations.Cmd;
import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.PixelCore.PixelCore;
import lombok.SneakyThrows;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.io.File;
import java.net.URLDecoder;
import java.nio.file.Files;

/**
 * @author joe 1/22/2015
 */
@Cmd(name="seticon",
    description = "Sets the icon of the server through a URL.",
    usage = "/seticon <url>",
    perm = "core.seticon",
    min = 1, max = 1)
public class SetIconCmd implements ICmd {

    @SneakyThrows
    public File getBukkitFolder() {
        String path = Bukkit.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        String decodedPath = URLDecoder.decode(path, "UTF-8");
        return new File(decodedPath).getParentFile();
    }
    
    @Override
    public boolean execute(final CommandSender sender, final ArgsSet args) throws PlayerException {
        PixelCore.getI().runNow(new Runnable() {
            @Override
            public void run() {
                HttpClient client = HttpClients.createDefault();
                HttpGet httpget = new HttpGet(args.get(0).getAsString());
                try {
                    sender.sendMessage(ChatColor.GREEN + "Downloading & Caching Server Icon from " + args.get(0).getAsString());
                    HttpResponse response = client.execute(httpget);
                    if (response.getStatusLine().getStatusCode() != 200) {
                        sender.sendMessage(ChatColor.RED + "Web error! Code: " + response.getStatusLine().getStatusCode() + " " + response.getStatusLine().getReasonPhrase());
                    }
                    File serverIconPath = new File(getBukkitFolder(), "server-icon.png");
                    if (serverIconPath.exists()) {
                        serverIconPath.delete();
                    }
                    Files.copy(response.getEntity().getContent(), serverIconPath.toPath());
                    PixelCore.getI().getCachedConfig().setServerIconUrl(args.get(0).getAsString());
                    PixelCore.getI().getCachedConfig().setServerIcon(Bukkit.loadServerIcon(serverIconPath));
                    sender.sendMessage("Loaded server icon!");
                } catch (Exception e) {
                    throw new PlayerException(e.getMessage());
                }
            }
        },sender, ThreadLevel.ASYNC);
        return true;
    }
}
