package io.ibj.PixelCore.cmd;

import com.earth2me.essentials.Essentials;
import com.earth2me.essentials.User;
import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.ICmd;
import io.ibj.JLib.cmd.annotations.Cmd;
import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.PixelCore.BigDecimalUtils;
import io.ibj.PixelCore.PixelCore;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.*;

/**
 * @author joe 3/13/2015
 */
@Cmd(name = "baltop",
description="Returns the top 10 balances of all players online.",
usage="/baltop",
max = 0,
        perm="core.baltop",
aggressive = true)
public class BalTopCmd implements ICmd {
    @Override
    public boolean execute(CommandSender sender, ArgsSet args) throws PlayerException {
        Essentials essentials = Essentials.getPlugin(Essentials.class);
        List<User> balSortedList = new ArrayList<>(Bukkit.getOnlinePlayers().size());
        for(Player player : Bukkit.getOnlinePlayers()){
            balSortedList.add(essentials.getUser(player));
        }
        Collections.sort(balSortedList, new Comparator<User>() {
            @Override
            public int compare(User o1, User o2) {
                return o2.getMoney().compareTo(o1.getMoney());
            }
        });

        PixelCore.getI().getF("baltop.header").replace("<count>",balSortedList.size() > 10 ? 10 : balSortedList.size()).sendTo(sender);


        for(int i = 0; i<10 && i<balSortedList.size(); i++){
            User user = balSortedList.get(i);
            PixelCore.getI().getF("baltop.filler").replace("<player>",user.getName()).replace("<bal>",user.getMoney()).replace("<sbal>",getShortBigDecimal(user.getMoney(),bigDecimalLength(user.getMoney()))).replace("<counter>",i+1).sendTo(sender);
        }
        PixelCore.getI().getF("baltop.footer").sendTo(sender);
        return true;
    }

    private String getShortBigDecimal(BigDecimal bal, int numberLength) {
        String shortBal;
        if(numberLength < 4){
            shortBal = bal.round(new MathContext(3)).toPlainString();
        }
        else if(numberLength < 7){
            shortBal = bal.divide(new BigDecimal(1000),new MathContext(3)).toPlainString()+"K";
        }
        else if(numberLength < 10){
            shortBal = bal.divide(new BigDecimal(1000000),new MathContext(3)).toPlainString()+"M";
        }
        else if(numberLength < 13){
            shortBal = bal.divide(new BigDecimal(1000000000),new MathContext(3)).toPlainString()+"B";
        }
        else
        {
            shortBal = bal.divide(new BigDecimal(1000000000000L), new MathContext(3)).toPlainString()+"T";
        }
        return shortBal;
    }
    private int bigDecimalLength(BigDecimal bal) {
        if(bal.signum() == 0){
            return 1;
        }
        double v = Math.floor(BigDecimalUtils.ln(bal, bal.scale()).doubleValue() / Math.log(10)) + 1;
        return (int) v;
    }
}
