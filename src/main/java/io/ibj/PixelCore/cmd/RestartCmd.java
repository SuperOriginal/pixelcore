package io.ibj.PixelCore.cmd;

import io.ibj.JLib.ThreadLevel;
import io.ibj.JLib.TimePeriod;
import io.ibj.JLib.TimeUnit;
import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.ICmd;
import io.ibj.JLib.cmd.annotations.Cmd;
import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.JLib.format.Format;
import io.ibj.PixelCore.PixelCore;
import io.ibj.PixelCore.player.APlayer;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

/**
 * @author joe 1/23/2015
 */
@Cmd(name="restart",
        aliases = {"reboot"},
    description = "Counts down a server before finally shutting down the server.",
    usage = "/restart [cancel]",
    perm = "core.restart",
    max = 1,
    aggressive = true)
public class RestartCmd implements ICmd{
    
    BukkitTask restartTask = null;
    static Runnable restartRunnable = RestartRunnable.invoke();
    static Runnable shutdownRunnable = RestartRunnable.ShutdownRunnable.invoke();
    static Runnable kickRunnable = new RestartRunnable.KickRunnable().invoke();

    @Override
    public boolean execute(CommandSender commandSender, ArgsSet argsSet) throws PlayerException {
        if(argsSet.size() != 0){
            if(argsSet.get(0).getAsString().equalsIgnoreCase("cancel")) {
                if (restartTask != null) {
                    restartTask.cancel();
                    PixelCore.getI().getF("restart.cancel").sendTo(Bukkit.getOnlinePlayers().toArray(new Player[Bukkit.getOnlinePlayers().size()]));
                    restartTask = null;
                    PixelCore.getI().getLockManager().setRestartLock(false);
                    return true;
                }
                else
                {
                    throw new PlayerException("The server was not restarting.");
                }
            }
        }
        else
        {
            PixelCore.getI().getLockManager().setRestartLock(true);
            restartTask = PixelCore.getI().runScheduled(restartRunnable, ThreadLevel.ASYNC,new TimePeriod(0, TimeUnit.TICKS),new TimePeriod(1,TimeUnit.SECONDS));
        }
        return true;
    }

    private static class RestartRunnable {
        private static Runnable invoke() {
            return new Runnable() {
                int restartSecondsLeft = 31;
                public void sendPeriodic(){
                    String title = PixelCore.getI().getFormatRaw("restart.periodic").replaceAll("<seconds>", String.valueOf(restartSecondsLeft));
                    String subtitle = PixelCore.getI().getFormatRaw("restart.periodicSubtitle").replaceAll("<seconds>", String.valueOf(restartSecondsLeft));
                    for(APlayer player : PixelCore.getI().getPlayerManager().getOnlinePlayers()){
                        player.getTitleManager().sendPopup(title,subtitle);
                    }
                    Bukkit.getConsoleSender().sendMessage(title);
                }

                @Override
                public void run() {
                    restartSecondsLeft--;
                    if(restartSecondsLeft < 1){
                        PixelCore.getI().runNow(kickRunnable, ThreadLevel.SYNC);
                        if(Bukkit.getOnlinePlayers().size() <= 0){
                            PixelCore.getI().runSync(shutdownRunnable);
                        }
                    }
                    if(restartSecondsLeft % 5 == 0){
                        sendPeriodic();
                    }
                    else if(restartSecondsLeft < 5){
                        sendPeriodic();
                    }
                }
            };
        }

        private static class ShutdownRunnable {
            private static Runnable invoke() {
                return new Runnable() {
                    @Override
                    public void run() {
                        Bukkit.getServer().shutdown();
                    }
                };
            }
        }

        private static class KickRunnable {

            public Runnable invoke() {
                return new Runnable() {
                    @Override
                    public void run() {
                        for(Player p : Bukkit.getOnlinePlayers().toArray(new Player[Bukkit.getOnlinePlayers().size()])){
                            p.kickPlayer(PixelCore.getI().getF("restart.kick").constructFancyMessage().get(0).toOldMessageFormat());
                        }
                    }
                };
            }
        }
    }
}
