package io.ibj.PixelCore.cmd;

import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.Executor;
import io.ibj.JLib.cmd.ICmd;
import io.ibj.JLib.cmd.annotations.Cmd;
import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.JLib.utils.Colors;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Iterator;

/**
 * @author joe 2/20/2015
 */
@Cmd(name = "kicknear",
    description = "Kicks all players near the executor, not including the executor.",
    usage = "/kicknear <radius> <message>",
    perm = "core.kicknear",
    min = 2,
    executors = {Executor.PLAYER})
public class KickNearCmd implements ICmd {
    @Override
    public boolean execute(CommandSender commandSender, ArgsSet argsSet) throws PlayerException {
        Player executor = ((Player) commandSender);
        double radius = argsSet.get(0).getAsDouble();
        String message = Colors.colorify(argsSet.joinArgs(1));
        Iterator<? extends Player> onlinePlayers = Bukkit.getWorld(executor.getWorld().getUID()).getPlayers().iterator();
        int kickedPlayers = 0;
        while(onlinePlayers.hasNext()){
            Player p = onlinePlayers.next();
            if(p != executor){
                if(p.getLocation().distance(executor.getLocation())<radius){
                    p.kickPlayer(message);
                    kickedPlayers++;
                }
            }
        }
        executor.sendMessage(ChatColor.RED+"Kicked "+kickedPlayers+" players!");
        return true;
    }
}
