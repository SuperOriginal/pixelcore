package io.ibj.PixelCore.cmd.announcer;

import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.ICmd;
import io.ibj.JLib.cmd.annotations.Cmd;
import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.PixelCore.PixelCore;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

/**
 * @author joe 2/22/2015
 */
@Cmd(name = "remove",
aliases = "rm",
description = "Removes a message from the announcer.",
usage = "/an rm <#>",
perm = "core.announcer.remove",
min = 1,
max = 1)
public class AnnouncerRemoveCmd implements ICmd{
    @Override
    public boolean execute(CommandSender commandSender, ArgsSet argsSet) throws PlayerException {
        Integer index = argsSet.get(0).getAsInteger();
        if(index < 1){
            throw new PlayerException("The index must be greater than 0.");
        }
        else if(index > PixelCore.getI().getAnnouncer().getMessageCount()){
            throw new PlayerException("The index must be less than "+PixelCore.getI().getAnnouncer().getMessageCount()+1);
        }
        index--;
        String message = PixelCore.getI().getAnnouncer().removeAnnounement(index);
        commandSender.sendMessage(ChatColor.GREEN+"Removed announcement: "+ChatColor.RESET+message);
        return true;
    }
}
