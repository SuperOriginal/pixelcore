package io.ibj.PixelCore.cmd.announcer;

import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.ICmd;
import io.ibj.JLib.cmd.annotations.Cmd;
import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.JLib.utils.Colors;
import io.ibj.PixelCore.PixelCore;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

/**
 * @author joe 2/22/2015
 */
@Cmd(name = "add",
description = "Adds a new announcement line to the announcer. May use Format syntax.",
usage = "/an add <message>",
perm = "core.announcer.add",
min = 1)
public class AnnouncerAddCmd implements ICmd{
    @Override
    public boolean execute(CommandSender commandSender, ArgsSet argsSet) throws PlayerException {
        String announcement = argsSet.joinArgs();
        PixelCore.getI().getAnnouncer().addAnnouncement(announcement);
        commandSender.sendMessage(ChatColor.GREEN+"Added new announcement: "+ChatColor.RESET+ Colors.colorify(announcement));
        return true;
    }
}
