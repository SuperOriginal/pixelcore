package io.ibj.PixelCore.cmd.announcer;

import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.ICmd;
import io.ibj.JLib.cmd.annotations.Cmd;
import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.PixelCore.PixelCore;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

/**
 * @author joe 2/22/2015
 */
@Cmd(name = "list",
description = "Lists all of the current announcer messages.",
usage = "/an list",
perm = "core.announcer.list",
max = 0)
public class AnnouncerListCmd implements ICmd {
    @Override
    public boolean execute(CommandSender commandSender, ArgsSet argsSet) throws PlayerException {
        commandSender.sendMessage(ChatColor.GREEN+"All of "+ PixelCore.getI().getAnnouncer().getMessageCount()+" announcement messages:");
        int index = 1;
        for(String s : PixelCore.getI().getAnnouncer().getMessages()){
            commandSender.sendMessage(ChatColor.GREEN+""+index+": "+ChatColor.RESET+s);
            index++;
        }
        return true;
    }
}
