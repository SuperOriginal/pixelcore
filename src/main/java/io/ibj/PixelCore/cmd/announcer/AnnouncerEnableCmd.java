package io.ibj.PixelCore.cmd.announcer;

import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.ICmd;
import io.ibj.JLib.cmd.annotations.Cmd;
import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.PixelCore.PixelCore;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

/**
 * @author joe 2/22/2015
 */
@Cmd(name = "enable",
description = "Sets whether the announcer should be enabled.",
usage = "/an enable [enable/disable]",
        perm = "core.announcer.enable",
        max = 1
)
public class AnnouncerEnableCmd implements ICmd {
    @Override
    public boolean execute(CommandSender commandSender, ArgsSet argsSet) throws PlayerException {
        if (argsSet.size() == 1) {
            boolean enable;
            String choice = argsSet.get(0).getAsString();
            if(choice.equalsIgnoreCase("enable")){
                enable = true;
            }
            else if(choice.equalsIgnoreCase("disable"))
            {
                enable = false;
            }
            else
            {
                return false;
            }
            PixelCore.getI().getAnnouncer().enable(enable);
        }
        commandSender.sendMessage(ChatColor.GREEN+"The announcer is "+ (PixelCore.getI().getAnnouncer().isEnabled()?ChatColor.DARK_GREEN+"enabled":ChatColor.RED+"disabled"));
        return true;
    }
}
