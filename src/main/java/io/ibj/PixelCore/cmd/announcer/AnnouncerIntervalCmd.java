package io.ibj.PixelCore.cmd.announcer;

import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.ICmd;
import io.ibj.JLib.cmd.annotations.Cmd;
import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.PixelCore.PixelCore;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

/**
 * @author joe 2/22/2015
 */
@Cmd(name = "interval",
description = "Sets the interval in seconds of the announcer.",
usage = "/an interval [seconds]",
perm = "core.announcer.interval",
min = 0,
max = 1)
public class AnnouncerIntervalCmd implements ICmd {
    @Override
    public boolean execute(CommandSender commandSender, ArgsSet argsSet) throws PlayerException {
        if(argsSet.size() == 0){
            commandSender.sendMessage(ChatColor.GREEN+"Current announcer interval: "+ChatColor.DARK_GREEN+ PixelCore.getI().getAnnouncer().getInterval());
        }
        else
        {
            Integer newInterval = argsSet.get(0).getAsInteger();
            if(newInterval < 1){
                throw  new RuntimeException("The interval must be greater than 1!");
            }
            PixelCore.getI().getAnnouncer().setInterval(newInterval);
            commandSender.sendMessage(ChatColor.GREEN+"Set new announcer interval: "+newInterval);
        }
        return true;
    }
}
