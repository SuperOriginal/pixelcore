package io.ibj.PixelCore.cmd;

import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.Executor;
import io.ibj.JLib.cmd.ICmd;
import io.ibj.JLib.cmd.annotations.Cmd;
import io.ibj.JLib.cmd.args.Arg;
import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.PixelCore.PixelCore;
import io.ibj.PixelCore.player.APlayer;
import io.ibj.PixelCore.player.Stats;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.joda.time.Instant;

/**
 * @author joe 2/22/2015
 */
@Cmd(name = "prestige",
description = "Prestiges a player, moving them from free rank to A.",
usage = "/prestige",
max = 0,
        perm = "core.prestige",
executors = Executor.PLAYER)
public class PrestigeCmd implements ICmd{
    @Override
    public boolean execute(CommandSender commandSender, ArgsSet argsSet) throws PlayerException {
        Player player = ((Player) commandSender);
        boolean isWithinFreeGroup = false;
        for (String group : PixelCore.getI().getPermission().getPlayerGroups(player)) {
            if(group.equalsIgnoreCase("free")){
                isWithinFreeGroup = true;
                break;
            }
        }
        if(!isWithinFreeGroup){
            throw new PlayerException(PixelCore.getI().getF("prestige.notFree"));
        }

        APlayer aPlayer = PixelCore.getI().getPlayerManager().getPlayer(player);

        if(!aPlayer.getCooldownManager().cooldown("prestige", Instant.now().plus(10000),false,false)){
            PixelCore.getI().getF("prestige.acknowledge").sendTo(commandSender);
            return true;
        }

        for(String command : PixelCore.getI().getCachedConfig().getPrestigeCommands()){
            command = command.replace("<name>",player.getName());
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(),command);
        }
        aPlayer.getAttribute(Stats.class).incPrestige();
        PixelCore.getI().getF("prestige.prestiged").replace("<level>",aPlayer.getAttribute(Stats.class).getPrestige());
        return true;
    }
}
