package io.ibj.PixelCore.cmd;

import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.Executor;
import io.ibj.JLib.cmd.TreeCmd;
import io.ibj.JLib.cmd.annotations.Cmd;
import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.PixelCore.PixelCore;
import io.ibj.PixelCore.cmd.token.TokenAddCmd;
import io.ibj.PixelCore.cmd.token.TokenGetCmd;
import io.ibj.PixelCore.cmd.token.TokenSetCmd;
import io.ibj.PixelCore.cmd.token.TokensRemoveCmd;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * @author joe 3/15/2015
 */
@Cmd(name = "token",
description= "Opens the tokens GUI.",
usage = "/token", aliases = {"tokens"},
        perm = "core.token")
public class TokenCmd extends TreeCmd {


    public TokenCmd(){
        registerCmd(TokenAddCmd.class);
        registerCmd(TokenGetCmd.class);
        registerCmd(TokenSetCmd.class);
        registerCmd(TokensRemoveCmd.class);
    }

    @Override
    public boolean executeIfNoSubFound(CommandSender sender, ArgsSet args) throws PlayerException {
        PixelCore.getI().getCachedConfig().getTokenMenu().showToPlayer((Player) sender);
        return true;
    }
}
