package io.ibj.PixelCore.cmd;

import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.ICmd;
import io.ibj.JLib.cmd.annotations.Cmd;
import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.JLib.format.Format;
import io.ibj.JLib.utils.StringUtils;
import io.ibj.PixelCore.PixelCore;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * @author joe 2/26/2015
 */
@Cmd(name ="list",
description = "Lists an online player count.",
usage = "/list",
perm = "core.list",
max = 0,
aggressive = true)
public class ListCmd implements ICmd {
    @Override
    public boolean execute(CommandSender commandSender, ArgsSet argsSet) throws PlayerException {
        String[] rawFormat = PixelCore.getI().getFormatList("list.format");
        String staffReplace;
        Set<Player> staffMembers = new HashSet<>();
        for(Player p : Bukkit.getOnlinePlayers()){
            if(p.hasPermission("core.staff")){
                staffMembers.add(p);
            }
        }
        if(staffMembers.isEmpty()){
            staffReplace = PixelCore.getI().getFormatRaw("list.none");
        }
        else
        {
            String[] players = new String[staffMembers.size()];
            String playerFormat = PixelCore.getI().getFormatRaw("list.instance");
            String seperator = PixelCore.getI().getFormatRaw("list.seperator");
            Iterator<Player> playerIterator = staffMembers.iterator();
            for(int i = 0; i<staffMembers.size(); i++){
                players[i] = playerFormat.replaceAll("<name>",playerIterator.next().getName());
            }
            staffReplace = StringUtils.joinList(seperator,players);
        }
        replace(rawFormat,"<staff>",staffReplace);

        Format format = new Format(rawFormat);
        format.replace("<online_players>",Bukkit.getOnlinePlayers().size());
        format.replace("<max_players>",Bukkit.getMaxPlayers());

        format.sendTo(commandSender);
        return true;
    }

    private void replace(String[] array, String key, String replace){
        for(int i = 0; i<array.length; i++){
            array[i] = array[i].replaceAll(key,replace);
        }
    }
}
