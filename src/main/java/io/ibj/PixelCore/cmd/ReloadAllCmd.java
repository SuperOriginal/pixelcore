package io.ibj.PixelCore.cmd;

import io.ibj.JLib.JPlug;
import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.ICmd;
import io.ibj.JLib.cmd.annotations.Cmd;
import io.ibj.JLib.exceptions.PlayerException;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;

/**
 * @author joe 1/22/2015
 */
@Cmd(name = "reloadconfigs", 
        description = "Reloads all configs on the server that are JPlugins.", 
        usage = "/reloadconfigs",
        perm = "core.reloadconfigs",
        max = 0)
public class ReloadAllCmd implements ICmd {
    @Override
    public boolean execute(CommandSender sender, ArgsSet args) throws PlayerException {
        for(Plugin plugin : Bukkit.getPluginManager().getPlugins()){
            if(plugin instanceof JPlug){
                ((JPlug) plugin).reload();
                sender.sendMessage(ChatColor.GREEN+"Reloaded configs for "+ChatColor.AQUA+plugin.getName());
            }
        }
        return true;
    }
}
