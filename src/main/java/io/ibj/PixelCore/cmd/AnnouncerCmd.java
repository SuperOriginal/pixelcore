package io.ibj.PixelCore.cmd;

import io.ibj.JLib.cmd.TreeCmd;
import io.ibj.JLib.cmd.annotations.Cmd;
import io.ibj.PixelCore.cmd.announcer.*;

/**
 * @author joe 2/20/2015
 */
@Cmd(name = "announcer",
aliases ={"an", "announce"},
description = "Root for all announcer commands.",
usage = "/an",
perm = "core.announcer")
public class AnnouncerCmd extends TreeCmd {
    public AnnouncerCmd(){
        registerCmd(AnnouncerAddCmd.class);
        registerCmd(AnnouncerEnableCmd.class);
        registerCmd(AnnouncerIntervalCmd.class);
        registerCmd(AnnouncerListCmd.class);
        registerCmd(AnnouncerRemoveCmd.class);
    }
}
