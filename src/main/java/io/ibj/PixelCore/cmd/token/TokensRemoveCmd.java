package io.ibj.PixelCore.cmd.token;

import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.ICmd;
import io.ibj.JLib.cmd.annotations.Cmd;
import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.PixelCore.PixelCore;
import io.ibj.PixelCore.player.APlayer;
import io.ibj.PixelCore.player.Stats;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.CommandSender;

/**
 * @author joe 3/15/2015
 */
@Cmd(name = "remove",
description = "Removes tokens from a player.",
usage = "/token remove <player> <amount>",
perm = "core.token.remove",
min = 2,
max = 2)
public class TokensRemoveCmd implements ICmd {
    @Override
    public boolean execute(CommandSender sender, ArgsSet args) throws PlayerException {
        APlayer player = PixelCore.getI().getPlayerManager().getOfflinePlayer(args.get(0).getAsString());
        if(player == null){
            throw new PlayerException("Player "+args.get(0).getAsString()+" has not played here before.");
        }
        int amount = args.get(1).getAsInteger();
        if(amount < 0){
            throw new PlayerException("Amount must be positive.");
        }

        player.getAttribute(Stats.class).withdrawTokens(amount);
        sender.sendMessage(ChatColor.GREEN+"You have removed "+amount+" tokens from "+player.getName()+".");
        return true;
    }
}
