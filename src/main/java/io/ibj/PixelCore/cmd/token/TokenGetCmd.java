package io.ibj.PixelCore.cmd.token;

import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.ICmd;
import io.ibj.JLib.cmd.annotations.Cmd;
import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.PixelCore.PixelCore;
import io.ibj.PixelCore.player.APlayer;
import io.ibj.PixelCore.player.Stats;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.CommandSender;

/**
 * @author joe 3/15/2015
 */
@Cmd(name = "get",
description = "Retrieves the amount of tokens the player has.",
usage = "/token get <player>",
perm = "core.token.get",
min = 1,
max = 1)
public class TokenGetCmd implements ICmd {
    @Override
    public boolean execute(CommandSender sender, ArgsSet args) throws PlayerException {
        APlayer player = PixelCore.getI().getPlayerManager().getOfflinePlayer(args.get(0).getAsString());
        if(player == null){
            throw new PlayerException("Player "+args.get(0).getAsString()+" has not played here before.");
        }

        sender.sendMessage(ChatColor.GREEN+player.getName()+ " has "+player.getAttribute(Stats.class).getTokens()+" tokens.");
        return true;
    }
}
