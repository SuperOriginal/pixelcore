package io.ibj.PixelCore.cmd;

import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.ICmd;
import io.ibj.JLib.cmd.annotations.Cmd;
import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.JLib.format.Format;
import io.ibj.JLib.utils.StringUtils;
import io.ibj.PixelCore.PixelCore;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.*;

/**
 * @author joe 2/28/2015
 */
@Cmd(name = "who",
description = "Shows all players online within the group specified.",
usage = "/who <group>",
perm = "core.who",
min = 0,
max = 1)
public class WhoCmd implements ICmd {
    @Override
    public boolean execute(CommandSender sender, ArgsSet args) throws PlayerException {
        if(args.size() == 0){
            String[] rawFormat = PixelCore.getI().getFormatList("list.format");
            String staffReplace;
            Set<Player> staffMembers = new HashSet<>();
            for(Player p : Bukkit.getOnlinePlayers()){
                if(p.hasPermission("core.staff")){
                    staffMembers.add(p);
                }
            }
            if(staffMembers.isEmpty()){
                staffReplace = PixelCore.getI().getFormatRaw("list.none");
            }
            else
            {
                String[] players = new String[staffMembers.size()];
                String playerFormat = PixelCore.getI().getFormatRaw("list.instance");
                String seperator = PixelCore.getI().getFormatRaw("list.seperator");
                Iterator<Player> playerIterator = staffMembers.iterator();
                for(int i = 0; i<staffMembers.size(); i++){
                    players[i] = playerFormat.replaceAll("<name>",playerIterator.next().getName());
                }
                staffReplace = StringUtils.joinList(seperator,players);
            }
            replace(rawFormat,"<staff>",staffReplace);

            Format format = new Format(rawFormat);
            format.replace("<online_players>",Bukkit.getOnlinePlayers().size());
            format.replace("<max_players>",Bukkit.getMaxPlayers());

            format.sendTo(sender);
            return true;
        }
        String group = args.get(0).getAsString();
        List<String> players = new LinkedList<>();
        String playerReplace = PixelCore.getI().getFormatRaw("who.instance");
        for(Player p : Bukkit.getOnlinePlayers()){
            if(PixelCore.getI().getPermission().playerInGroup(p,group)){
                players.add(playerReplace.replaceAll("<player>",p.getName()));
            }
        }
        String replacement;
        if(players.isEmpty()){
            replacement = PixelCore.getI().getFormatRaw("who.none");
        }
        else
        {
            replacement = StringUtils.joinList(PixelCore.getI().getFormatRaw("who.separator"),players);
        }
        String[] k = PixelCore.getI().getFormatList("who.format");
        replace(k,"<players>",replacement);
        replace(k,"<group>",group);
        new Format(k).sendTo(sender);
        return true;
    }

    public void replace(String[] source, String key, String replace){
        for (int i = 0; i < source.length; i++) {
            source[i] = source[i].replaceAll(key,replace);
        }
    }
}
