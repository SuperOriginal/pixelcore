package io.ibj.PixelCore.cmd;

import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.ICmd;
import io.ibj.JLib.cmd.annotations.Cmd;
import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.PixelCore.PixelCore;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

/**
 * @author joe 1/22/2015
 */
@Cmd(name = "setmotd",
    description = "Changes the MOTD realtime.",
    usage = "/setmotd <1/2> <line>",
    perm = "core.setmotd",
    min = 2)
public class SetMotdCmd implements ICmd {
    @Override
    public boolean execute(CommandSender commandSender, ArgsSet argsSet) throws PlayerException {
        String line = argsSet.get(0).getAsString();
        if(line.equals("1")){
            PixelCore.getI().getCachedConfig().setMotdLine1(argsSet.joinArgs(1));
            commandSender.sendMessage(ChatColor.GREEN + "Set line 1 to: " + ChatColor.RESET + PixelCore.getI().getCachedConfig().getLine1());
        }
        else if(line.equals("2")){
            PixelCore.getI().getCachedConfig().setMotdLine2(argsSet.joinArgs(1));
            commandSender.sendMessage(ChatColor.GREEN+"Set line 2 to: "+ChatColor.RESET+PixelCore.getI().getCachedConfig().getLine2());
        }
        else
        {
            return false;
        }
        return true;
    }
}
