package io.ibj.PixelCore.cmd;

import com.earth2me.essentials.utils.DateUtil;
import io.ibj.JLib.JLib;
import io.ibj.JLib.ThreadLevel;
import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.ICmd;
import io.ibj.JLib.cmd.annotations.Cmd;
import io.ibj.JLib.exceptions.PlayerException;
import io.ibj.JLib.format.TagStyle;
import io.ibj.JLib.utils.UUIDUtils;
import io.ibj.PixelCore.PixelCore;
import io.ibj.PixelCore.player.SqlAPlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.sql.*;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by Aaron.
 */
@Cmd(name="seen",
        description = "View a player's last seen time.",
        usage = "/seen <player>",
        perm = "pixelcore.seen",
        max = 1,
        aggressive = true)
public class SeenCmd implements ICmd{
    String name;
    @Override
    public boolean execute(CommandSender sender, ArgsSet args) throws PlayerException {
        SqlAPlayer target;

        if(args.size() == 0){
            if(!(sender instanceof Player)){
                throw new PlayerException("Must be a player to execute with no argument!");
            }
            target = (SqlAPlayer)PixelCore.getI().getPlayerManager().getPlayer((Player) sender);
            name = sender.getName();
        }
        else
        {
            name = args.get(0).getAsString();
            target = (SqlAPlayer)PixelCore.getI().getPlayerManager().getOfflinePlayer(args.get(0).getAsString());
            if(target == null){
                    PixelCore.getI().runNow(new Runnable() {
                        @Override
                        public void run() {
                            try (Connection con = PixelCore.getI().getDBConnection()){
                                PreparedStatement stmt = con.prepareStatement("SELECT * from player_stats where playerId = ?");
                                stmt.setBytes(1,UUIDUtils.toBytes(JLib.getI().getPlayerLookup().getFromName(name)));
                                ResultSet set = stmt.executeQuery();
                                if (set.next()) {
                                            message(sender, set);
                                            stmt.close();
                                } else {
                                    new PlayerException("That player has never played here before!").throwToPlayer(sender);
                                }
                            }catch (SQLException e){
                                e.printStackTrace();
                            }
                        }
                    }, ThreadLevel.ASYNC);
                return true;
            }
        }
        message(sender,target);

        return true;
    }

    public String seen(String name){
        SqlAPlayer p = (SqlAPlayer) PixelCore.getI().getPlayerManager().getOfflinePlayer(name);
        Timestamp stamp = p.getPlayed();

        return formatDateDiff(stamp.getTime());
    }

    public void message(CommandSender sender,SqlAPlayer target){
        String time = seen(target.getName());

        if(target.getOnlinePlayer() != null) {
            PixelCore.getI().getF("seen.online").replace("<player>", target.getName())
                    .replace("<time>",time)
                    .replace("<rawtime>", target.getPlayed().toString())
                    .setTagStyle(TagStyle.EVERY_LINE).sendTo(sender);
        }else{
            PixelCore.getI().getF("seen.offline").replace("<player>", target.getName())
                    .replace("<rawtime>", target.getPlayed().toString())
                    .replace("<time>",time)
                    .setTagStyle(TagStyle.EVERY_LINE).sendTo(sender);
        }
        if(sender.hasPermission("pixelcore.seen.admin")){
            PixelCore.getI().getF("seen.admin").replace("<ip>", target.getOnlinePlayer() != null ? target.getOnlinePlayer().getAddress().getAddress().toString().replace("/","") : target.getIp())
                    .setTagStyle(TagStyle.EVERY_LINE).sendTo(sender);
        }
    }

    public void message(CommandSender sender,ResultSet set){
        try {
            if(set == null || set.getTimestamp("timestamp") == null || set.getString("ip") == null){
                if(sender != null) new PlayerException("The IP and Timestamp for this user have not been stored yet.").throwToPlayer(sender);
                return;
            }
            String time = formatDateDiff(set.getTimestamp("timestamp").getTime());
            PixelCore.getI().getF("seen.offline").replace("<player>", JLib.getI().getPlayerLookup().getFromUUID(UUIDUtils.fromBytes(set.getBytes("playerId"))))
                    .replace("<rawtime>", set.getTimestamp("timestamp").toString())
                    .replace("<time>", time)
                    .setTagStyle(TagStyle.EVERY_LINE).sendTo(sender);
            if (sender.hasPermission("pixelcore.seen.admin")) {
                PixelCore.getI().getF("seen.admin").replace("<ip>", set.getString("ip"))
                        .setTagStyle(TagStyle.EVERY_LINE).sendTo(sender);
            }
            set.close();
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }



    //------------------------------ Date Formatting Utils Acquired from Essentials -------------------------------------------------------
    public static String formatDateDiff(long date)
    {
        Calendar c = new GregorianCalendar();
        c.setTimeInMillis(date);
        Calendar now = new GregorianCalendar();
        return DateUtil.formatDateDiff(now, c);
    }

    public static String formatDateDiff(Calendar fromDate, Calendar toDate)
    {
        boolean future = false;
        if (toDate.equals(fromDate))
        {
            return ("now");
        }
        if (toDate.after(fromDate))
        {
            future = true;
        }
        StringBuilder sb = new StringBuilder();
        int[] types = new int[]
                {
                        Calendar.YEAR, Calendar.MONTH, Calendar.DAY_OF_MONTH, Calendar.HOUR_OF_DAY, Calendar.MINUTE, Calendar.SECOND
                };
        String[] names = new String[]
                {
                        "year", "years", "month", "months", "day", "days", "hour", "hours", "minute", "minutes", "second", "seconds"
                };
        int accuracy = 0;
        for (int i = 0; i < types.length; i++)
        {
            if (accuracy > 2)
            {
                break;
            }
            int diff = dateDiff(types[i], fromDate, toDate, future);
            if (diff > 0)
            {
                accuracy++;
                sb.append(" ").append(diff).append(" ").append(names[i * 2 + (diff > 1 ? 1 : 0)]);
            }
        }
        if (sb.length() == 0)
        {
            return "now";
        }
        return sb.toString().trim();
    }
    static int dateDiff(int type, Calendar fromDate, Calendar toDate, boolean future)
    {
        int diff = 0;
        long savedDate = fromDate.getTimeInMillis();
        while ((future && !fromDate.after(toDate)) || (!future && !fromDate.before(toDate)))
        {
            savedDate = fromDate.getTimeInMillis();
            fromDate.add(type, future ? 1 : -1);
            diff++;
        }
        diff--;
        fromDate.setTimeInMillis(savedDate);
        return diff;
    }



}
