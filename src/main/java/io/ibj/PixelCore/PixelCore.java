package io.ibj.PixelCore;

import io.ibj.JLib.*;
import io.ibj.JLib.db.AuthenticatedDatabaseConnectionDetails;
import io.ibj.JLib.db.DatabaseConnectionDetails;
import io.ibj.JLib.db.DatabaseType;
import io.ibj.JLib.db.utils.ScriptRunner;
import io.ibj.JLib.file.ResourceFile;
import io.ibj.JLib.file.ResourceReloadHook;
import io.ibj.JLib.file.YAMLFile;
import io.ibj.JLib.utils.Colors;
import io.ibj.PixelCore.cmd.*;
import io.ibj.PixelCore.listener.*;
import io.ibj.PixelCore.menu.MenuRegistrar;
import io.ibj.PixelCore.menu.PageReplacer;
import io.ibj.PixelCore.menu.ReplacementDelegate;
import io.ibj.PixelCore.player.*;
import lombok.Getter;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;

import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.Connection;
import java.util.List;

/**
 * @author joe 2/20/2015
 */
public class PixelCore extends JPlug {

    @Getter
    private static PixelCore i;

    @Override
    protected void onModuleEnable() throws Exception {
        i = this;

        RegisteredServiceProvider<Permission> permissionProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.permission.Permission.class);
        if (permissionProvider != null) {
            permission = permissionProvider.getProvider();
        }

        registerResource(new YAMLFile(this, "database.yml", new ResourceReloadHook() {
            @Override
            public void onReload(ResourceFile file) {
                FileConfiguration config = ((YAMLFile) file).getConfig();
                String ip = config.getString("host", "localhost");
                Integer port = config.getInt("port", 3306);
                String database = config.getString("database", "");
                if (config.contains("username")) {
                    String username = config.getString("username");
                    String password = config.getString("password");
                    databaseConnection = new AuthenticatedDatabaseConnectionDetails<>(DatabaseType.SQL, Connection.class, ip, port, database, username, password);
                } else {
                    databaseConnection = new DatabaseConnectionDetails<>(DatabaseType.SQL, Connection.class, ip, port, database);
                }
            }
        }));
        menuRegistrar = new MenuRegistrar();
        menuRegistrar.addReplacementDelegate(new ReplacementDelegate() {
            @Override
            public void replace(PageReplacer delegate, Player player) {
                delegate.replace("<name>",player.getName());
            }
        });
        menuRegistrar.addReplacementDelegate(new ReplacementDelegate() {
            @Override
            public void replace(PageReplacer delegate, Player player) {
                delegate.replace("<tokens>",PixelCore.getI().getPlayerManager().getPlayer(player).getAttribute(Stats.class).getTokens());
            }
        });
        registerResource(new YAMLFile(this, "menu.yml", menuRegistrar));
        cachedConfig = new CachedConfig();
        registerResource(new YAMLFile(this, "config.yml", cachedConfig));
        announcer = new AnnouncerManager();
        registerResource(new YAMLFile(this,"announcer.yml",announcer));
        reload();
        ScriptRunner runner = new ScriptRunner(getDBConnection(), true, true);
        try (Reader reader = new InputStreamReader(getResource("init.sql"))) {
            runner.runScript(reader);
        }
        attributeRegistrar = new AttributeRegistrar();
        statsManager = new StatsManager();
        attributeRegistrar.registerAttributeManager(statsManager);
        playerManager = new SqlPlayerManager();
        registerCmd(ReloadAllCmd.class);
        registerCmd(RestartCmd.class);
        registerCmd(SetIconCmd.class);
        registerCmd(SetMotdCmd.class);
        registerCmd(StatsCmd.class);
        registerCmd(KickNearCmd.class);
        registerCmd(AnnouncerCmd.class);
        registerCmd(PrestigeCmd.class);
        registerCmd(ListCmd.class);
        registerCmd(WhoCmd.class);
        registerCmd(BalTopCmd.class);
        registerCmd(TokenCmd.class);
        registerCmd(SeenCmd.class);
        registerEvents(new StatsListener());
        registerEvents(new ConnectionListener());
        registerEvents(new PingListener());
        registerEvents(new ChatListener());
        registerEvents(new BalanceListener());
        registerEvents(new ActionBarMovementListener());
        registerEvents(new KillstreakListener());
        registerEvents(new BedFixListener());
        final WarpListener warpListener = new WarpListener();
        registerEvents(warpListener);
        runNow(new Runnable() {
            @Override
            public void run() {
                warpListener.calculateAcceptedWarpCommands();
            }
        },ThreadLevel.SYNC);
        runLater(new Runnable() {
            @Override
            public void run() {
                getLockManager().setStartupLock(false);
            }
        }, ThreadLevel.SYNC, new TimePeriod(5,TimeUnit.SECONDS));
        runScheduled(statsManager, ThreadLevel.ASYNC, new TimePeriod(1, TimeUnit.MINUTES), new TimePeriod(1, TimeUnit.MINUTES));
        runScheduled(announcer, ThreadLevel.SYNC, new TimePeriod(1, TimeUnit.SECONDS), new TimePeriod(1,TimeUnit.SECONDS));
        runScheduled(new ActionBarRunner(), ThreadLevel.SYNC, new TimePeriod(1, TimeUnit.SECONDS), new TimePeriod(1, TimeUnit.SECONDS));
    }

    @Override
    protected void onModuleDisable() throws Exception {
        for (Player p : Bukkit.getOnlinePlayers()) {
            p.kickPlayer(PixelCore.getI().getF("restart.reload").constructFancyMessage().get(0).toOldMessageFormat());
        }
    }

    @Getter
    private AttributeRegistrar attributeRegistrar;

    @Getter
    private MenuRegistrar menuRegistrar;

    private DatabaseConnectionDetails<Connection> databaseConnection;

    @Getter
    private CachedConfig cachedConfig;

    @Getter
    private PlayerManager playerManager;

    @Getter
    private StatsManager statsManager;

    @Getter
    private LockManager lockManager = new LockManager();

    @Getter
    private Permission permission;

    @Getter
    private AnnouncerManager announcer;

    public String[] getFormatList(String key){
        if(this.formatsFile.getConfig().isList(key)){
            List<String> ret = this.formatsFile.getConfig().getStringList(key);
            String[] retArray = new String[ret.size()];
            for(int i = 0; i < ret.size(); i++){
                retArray[i] = Colors.colorify(ret.get(i));
            }
            return retArray;
        }
        else
        {
            String[] ret = new String[1];
            ret[0] = Colors.colorify(this.formatsFile.getConfig().getString(key));
            return ret;
        }
    }

    public Connection getDBConnection() {
        return JLib.getI().getDatabaseManager().getConnection(databaseConnection);
    }


}