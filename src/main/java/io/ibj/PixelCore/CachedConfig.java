package io.ibj.PixelCore;

import com.google.common.collect.ImmutableList;
import io.ibj.JLib.ThreadLevel;
import io.ibj.JLib.file.ResourceFile;
import io.ibj.JLib.file.ResourceReloadHook;
import io.ibj.JLib.file.YAMLFile;
import io.ibj.JLib.utils.Colors;
import io.ibj.PixelCore.menu.Menu;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.util.CachedServerIcon;

import java.io.File;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * @author joe 1/22/2015
 */
public class CachedConfig implements ResourceReloadHook{
    
    YAMLFile file;
    
    @Getter
    String line1;
    @Getter
    String line2;
    @Getter
    String serverIconUrl;
    @Getter
    @Setter
    CachedServerIcon serverIcon;

    private List<String> prestigeCommands;
    @Getter
    private String prestigeFormat;

    @Getter
    private String balanceFormat;

    @Getter
    Set<TabListEntry> tabEntries;

    @Getter
    private String positiveBalFormat;

    @Getter
    private String negativeBalFormat;

    public List<String> getPrestigeCommands(){
        return ImmutableList.copyOf(prestigeCommands);
    }

    private List<WarpEntry> warpEntries;

    public List<WarpEntry> getWarpEntries(){
        return ImmutableList.copyOf(warpEntries);
    }

    public List<KillstreakEntry> killstreakEntries;

    public List<KillstreakEntry> getKillstreakEntries(){
        return ImmutableList.copyOf(killstreakEntries);
    }

    @Getter
    private Integer killstreakTimeout;

    @Getter
    private String mainMenuName;

    public Menu getTokenMenu(){
        return PixelCore.getI().getMenuRegistrar().getMenu(mainMenuName);
    }

    public void setServerIconUrl(String url){
        serverIconUrl = url;
        save();
    }

    public void setMotdLine1(String string){
        line1 = ChatColor.translateAlternateColorCodes('&',string);
        save();
    }
    
    public void setMotdLine2(String string){
        line2 = ChatColor.translateAlternateColorCodes('&', string);
        save();
    }
    
    public void save(){
        file.getConfig().set("motd.line1",line1);
        file.getConfig().set("motd.line2",line2);
        file.getConfig().set("motd.icon",serverIconUrl);
        PixelCore.getI().runNow(new Runnable() {
            @Override
            public void run() {
                file.saveConfig();
            }
        }, ThreadLevel.ASYNC);
    }
    
    @Override
    public void onReload(ResourceFile resourceFile) {
        file = ((YAMLFile) resourceFile);
        FileConfiguration config = ((YAMLFile) resourceFile).getConfig();
        line1 = ChatColor.translateAlternateColorCodes('&',config.getString("motd.line1"));
        line2 = ChatColor.translateAlternateColorCodes('&',config.getString("motd.line2"));
        serverIconUrl = config.getString("motd.icon");
        PixelCore.getI().runNow(new Runnable() {
            @Override
            public void run() {
                HttpClient client = HttpClients.createDefault();
                HttpGet httpget = new HttpGet(serverIconUrl);
                try {
                    HttpResponse response = client.execute(httpget);
                    File serverIconPath = new File(getBukkitFolder(), "server-icon.png");
                    if (serverIconPath.exists()) {
                        serverIconPath.delete();
                    }
                    Files.copy(response.getEntity().getContent(), serverIconPath.toPath());
                    PixelCore.getI().getCachedConfig().setServerIcon(Bukkit.loadServerIcon(serverIconPath));
                } catch (Exception e) {
                    PixelCore.getI().handleError(e);
                }
            }
        },ThreadLevel.ASYNC);
        tabEntries = new HashSet<>();
        for(String key : config.getConfigurationSection("tabList").getKeys(false)){
            tabEntries.add(new TabListEntry("core.tablist."+key,
                    Colors.colorify(config.getString("tabList."+key+".prefix","")),
                    Colors.colorify(config.getString("tabList."+key+".suffix","")),
                    config.getInt("tabList."+key+".priority")));
        }

        prestigeCommands = config.getStringList("prestige.cmds");
        prestigeFormat = Colors.colorify(config.getString("prestige.format"));
        balanceFormat = Colors.colorify(config.getString("balanceFormat"));
        positiveBalFormat = Colors.colorify(config.getString("balanceInterrupt.positive"));
        negativeBalFormat = Colors.colorify(config.getString("balanceInterrupt.negative"));
        ConfigurationSection section = config.getConfigurationSection("warp");
        warpEntries = new LinkedList<>();
        for(String key : section.getKeys(false)){
            WarpEntry entry = new WarpEntry();
            entry.name = key;
            entry.error = Colors.colorify(section.getString(key + ".error", "&4You don't have access to these warps!"));
            entry.warps = section.getStringList(key+".warps");
            warpEntries.add(entry);
        }
        killstreakTimeout = config.getInt("killStreak.timeout");
        ConfigurationSection killSection = config.getConfigurationSection("killStreak.killAmount");
        killstreakEntries = new LinkedList<>();
        for(String key : killSection.getKeys(false)){
            try{
                Integer kills = Integer.parseInt(key);
                String value = Colors.colorify(killSection.getString(key,""));
                KillstreakEntry entry = new KillstreakEntry();
                entry.amount = kills;
                entry.message = value;
                killstreakEntries.add(entry);
            }
            catch(NumberFormatException e){

            }
        }
        mainMenuName = config.getString("mainMenu");
    }

    @SneakyThrows
    public File getBukkitFolder() {
        String path = Bukkit.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        String decodedPath = URLDecoder.decode(path, "UTF-8");
        return new File(decodedPath).getParentFile();
    }

    public static class WarpEntry {

        @Getter
        String name;
        @Getter
        String error;
        @Getter
        List<String> warps;

    }

    public static class KillstreakEntry{
        @Getter
        int amount;
        @Getter
        String message;
    }
}
